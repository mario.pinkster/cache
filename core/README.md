This is the core functionality for the adaptive LRU cache. 
An example webapplication that uses this core can be found in the **razz** module.

* CachedBase and LRUCache together form the actual Cache object. You can define multiple cache objects in one application to
store different types of objects. Each cache object must be configured with at least a size (in number of objects), 
an expiration period and a unique identifying name. This name is then used to identify the cache in the cache registry which is 
used by the web interface (if present and used).
LRUCache is a generic type, when you create an instance you must specify the type of object that it will 
contain and this type must extend CachedItem. 
* CachedItem is the base class from which all cached object must be derived. 
It adds some timestamps that are necessary for the LRU cleanup algorithm.
* Deadpool is a specialized contained object that is used during the cleanup process in order to build a list of cached objects
that will be deleted from the cache.

The Cache works by calling its get() and put() methods. When you call put() while the cache is at maximum capacity, this will
trigger a cleanup procedure to run in the same thread that was calling the put() method for the cache.

The cleanup procedure will remove a varying number of items. Cached items are considered for removal when their time to live has
expired. In the case that there are not enough expired items to satisfy the requires amount of removals for one particular cleanup
run the list of items to delete will be completed by adding items that have not been used (gotten) from the cache for a long time.

The cleanup procedure is designed to keep an eye on time used while scanning the cache for items that may be deleted and 
quit the scanning prematurely when a predetermined amount of nanoseconds have been used up. The idea is to guarantee a cap
on the delay for cleanup when trying to put() a new item into a full cache.

The cleanup process measures how much work it was able to do and how long it took to do that work and adjusts its
parameters at the end of each run to find an optimal balance between number of items deleted and time it took 
to delete them. This is why I call it an *adaptive* cache.

The CacheBase is mainly concerned with the mechanics of getting and putting items and of gathering statistics on hits,
misses and expired items. The LRUCache is mainly concerned with the cleanup process and statistics thereof.

Statistics are logged at fixed time intervals.

The main parameters of a cache such as size and time to live for the items can be adjusted on runtime. Thus, you can
expand or shrink a cache without taking the whole application down. A webinterface exists that you can call in order
to view statistics on all caches and to change cache parameters for individual caches. This interface also allows for
viewing and filtering individual items (based on the items key in the cache) and for removing inidividual items from
the cache (or emptying the cache alltogether).

