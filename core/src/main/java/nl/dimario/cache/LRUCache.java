package nl.dimario.cache;

import java.security.InvalidParameterException;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This implementation of the cache base performs a cleanup by first building a list
 * of items that must be deleted and then deleting those items. The size of the list
 * is a parameter which may be altered slightly ("tuned") at the end of each cleanup run,
 * as a result of various measurements. The idea is to implement an autotuning
 * system that will gradually reach an ideal equilibrium between amount of items
 * to clean up (large is good) and the time it takes to do so (small is good).
 * 
 * The assumption is that it takes more time to build a large list of items to kill.
 * However, measurements indicate that the relation not always is linear.
 * More analysis would be needed to gain understanding of what is going on.
 * For the time being I will proceed as if the relationship does follow the
 * expectation that more cleanup takes more time.
 *
 * When building the list of items to delete (the "deadpool") we inspect the items in the
 * cache one by one and look for two things: 1 - is the item expired and 2 - if not expired,
 * when was it last used by the application. The best case is when we can satisfy the number
 * of items that must be deleted completely with expired items. But this is not always
 * possible, so the next best case is when all non expired items in the deadpool are
 * items that have not been used by the application for the longest time (these are not
 * the oldest items per se, but items that were not requested by the application for a long
 * time).
 * 
 * The scanning of items in the cache stops when one of three conditions is met:
 * (1) We have filled the deadpool to the required size with expired items.
 * (2) All items in the cache have been scanned. The deadpool contains at least one 
 * non expired item, the uexpired items have not been used for a long time, and the
 * deadpool is completely filled (the required number of items to delete has been found).
 * (3) The scanning process has run out of its allotted time and aborted prematurely.
 * In this case the number of items to delete in the deadpool may or may not be
 * the required number, and not all items in the cache have been scanned so there is no 
 * guarantee that the unexpired items in the deadpool fit the "longest not used" rule.
 * 
 * The reason why the scanning process did terminate is used in the subsequent adjustment
 * of the required number of items to place in the deadpool for the next cleanup run.
 * If the scan was aborted prematurely (reason 3) this means that there was too much work to do
 * in the allotted time so we reduce the amount of work.
 * If the scan terminated because the deadpool was completely filled with expired items (reason 1),
 * this means we had time to spare and could have added more items to the deadpool.
 * In that case we increase the number of items to place in the deadpool for the next run.
 * If we have scanned all items in the cache but did not find enough expired ones so the
 * deadpool also has unexpired items (reason 2), this means that other cache 
 * parameters should be tuned but this is not something that we will do automatically.
 * But because we had enough time to finish scanning the whole cache we will increase the
 * amount of work slightly.
 * 
 * Once the scanning phase of the cleanup process has collected a list of items 
 * that must be deleted, these items are deleted. For each individual deletion,
 * the data structure of the cache is locked briefly and then released (using
 * the standard Java synchronize() method).
 * 
 * A "busy" flag is raised at the start of the whole process in order to ensure that
 * no two threads will try and run a cleanup simultaneously.
 * 
 * @author Mario Pinkster
 */

public class LRUCache<T extends CachedItem> extends CacheBase<T> {

  private static Logger LOG = LoggerFactory.getLogger(LRUCache.class);

  /**
   * This is the "busy" flag.
   */
  private boolean cleanupRunning;
  
  /**
   * These are the possible reasons for ending a cleanup run 
   */
  private static enum ReasonTerminated  {
      AllExpired,
      ScanCompleted,
      Aborted
  };

  /**
   * The next three settings control the amount of work that will be performed during a cleanup.
   */
  
  /**
   * This is the number of MICRO seconds that we want the scanning part of one cleanup 
   * run to take at the most.
   * When scanning, we measure how long we are taking and if exceeding the 
   * number given here the scanning is aborted prematurely.
   * This value can be altered on runtime via the web interface.
   */
  private int maxCleanupDuration;

  /**
   * This is the number of items to be deleted that we will attempt to find 
   * during the scanning phase. It is tuned at the end of each cleanup run
   * in accordance to what transpired. You can also alter this value on 
   * runtime via the web interface.
   * In both cases (manual or autotune) the actual value is checked to 
   * remain within sensible limits. The lower boundary is hardcoded, the upper boundary
   * is a parameter of the cache (see next attribute).
   */
  private int removePerCleanup;

  /**
   * This is the upper boundary for the number of items to remove during any cleanup
   * run. It is set in te constructor to about ten percent of the cache size and
   * can be altered via the web interface.
   */
  private int maxRemovePerCleanup;

  
  /**
   * This counter counts the total number of cleanup runs that have been performed.
   */
  private AtomicInteger cleanupCount;

  /**
   * The rest of the attributes record some statistics about the last cleanup run that was performed.
   */
  
  /**
   * This is the measured duration in microseconds of the most recent cleanup run. 
   */
  private long lastCleanupDuration;
  
  /**
   * This is the average age of the items that were deleted from the cache at the most
   * recet cleanup run. "Age" being measured starting when the item was placed in the
   * cache and ending when the item was removed during cleanup.
   */
  private int avgCleanupAge;
  

  
  /**
   * The constructor/creator combination passes the given values on to the base class initializer
   * and then calculates some sensible values for the parameters that are
   * specific for this implementation of the cache.
   * 
   * The visibility of this constructor is "package" because we need to access it in the JUnit test.
   */
  
  LRUCache() {
    super();
    // Make sure we start off while not being in a cleanup run.
    cleanupRunning = false;
    // We initially want any cleanup run to not take more than this number of microseconds.
    this.maxCleanupDuration = 1750;
    // Initialize the number of cleanup runs
    this.cleanupCount  = new AtomicInteger(0);
    
  }

  /**
   * Create and initialize an instance of LRUCache. A static creator is used because exceptions
   * may occur during validation of various settings that arte applied to the newly constructed 
   * cache instance.
   */
  public static LRUCache<? extends CachedItem> create( String tag, int maxSize, int maxAgeSeconds) {
    
    LRUCache<? extends CachedItem> result = new LRUCache<>();
    result.init( tag, maxSize, maxAgeSeconds);
    
    // Initialize the parameters for autotuning of the cleanup process
    result.setAutotuneParams();
    
    // Make sure all parameters remain within sensible boundaries and report their values to the log.
    result.regularizeAndReportTuning();
    
    return result;
  }



  /**
   * Set sensible values for the autotuning parameters. This must happen any
   * time the max size of the cache changes.
   */
  private void setAutotuneParams() {
    
    // convenience value: five percent of the maximum size of the cache.
    int fivePercent = (5 * getMaxSize()) / 100;

    // Autotuning may not set the number of items to be deleted in one cleanup run
    // beyond this value (currently ten percent of max size).
    this.maxRemovePerCleanup = 2 * fivePercent;

    // In the very first cleanup run, we will try to delete about two or three percent 
    // of the items in the cache. NOTE: Integer division truncates the value so for
    // small numbers this is not very precise. 
    this.removePerCleanup = fivePercent / 2;
  }
  
  /**
   * When setting a new value for max size, we must recalculate some autotuning parameters.
   */
  @Override
  public void setMaxSize( int maxSizeValue) {
    super.setMaxSize(maxSizeValue);
    setAutotuneParams();
  }
  
  /**
   * Adjust the amount of work that will be done during the next cleanup run by looking
   * at how and why *this* cleanup run finished / stopped / was aborted.
   * 
   * @param deadpoolSize - this is the actual amount of items that was found during 
   * this cleanup run. It may be less than the required amount, this.removePerCleanup.
   * @param reasonScanStopped is a code for why the scan finished.
   * 1 - we found the required number and they were all expired.
   * 2 - we scanned all items in the cache and encounterd no other termination condition.
   * 3 - we ran out of time while scanning the items.
   */
  protected void autotune( int deadpoolSize, ReasonTerminated reasonScanStopped) {

    // delta is the calculated adjustment for this.removePerCleanup
    int delta = 0;
    switch( reasonScanStopped) {
      case AllExpired: {
        LOG.debug( "{} tuning : the required number of expired items were found.", getTag());
        delta = 4;
      } break;
      
      case ScanCompleted: {
        LOG.debug( "{} tuning : all items in the cache were scanned within the time limit.", getTag());
        delta = 1;
      } break;
      
      case Aborted: {
        LOG.debug( "{} tuning : scanning was aborted due to exceeding the time limit.", getTag());
        delta = (deadpoolSize -2) - removePerCleanup;
      } break;
    }

    removePerCleanup += delta;
    LOG.debug( "{} tuning : intended adjustment for the workload={}", getTag(), delta);
    
    // But make sure the number of items to cleanup stays within hard boundaries in order
    // to prevent runaway or do-nothing behaviour.
    regularizeAndReportTuning();
  }

  /**
   * Check the number of items to cleanup in the next run against upper and lower boundaries
   * and report the new value. 
   */
  private void regularizeAndReportTuning() {

    String rangeMsg = "";
    if (removePerCleanup > this.maxRemovePerCleanup) {
      removePerCleanup = this.maxRemovePerCleanup;
      rangeMsg = ", upper boundary reached";
    }
    if (removePerCleanup < 3) {
      removePerCleanup = 3;
      rangeMsg = ", lower boundary reached";
    }

    // Report the value after having regularized it
    LOG.info("{} tuning : new value of removePerCleanup={}{}", getTag(), this.removePerCleanup, rangeMsg);
  }

  /**
   * This method is a wrapper around the actual cleanup process that interacts with the
   * "cleanup in progress" flag to determine wether an actual cleanup run can be executed.
   */

  @Override
  protected void cleanup() {
    
      if( isCleaningUp()) {
        LOG.info( "{} cleanup: already running, this call is ignored", getTag());
        return;
      }

      // try .. finally ensures that no matter what goes wrong, the "cleanup in progress"
      // flag is always reset to false.
      try {
        setCleaningUp( true);
        cleanupWork();
      } finally {
        setCleaningUp( false);
      }
  }
  
  @Override
  synchronized protected boolean isCleaningUp() {
    return cleanupRunning;
  }
  
  synchronized void setCleaningUp( boolean value) {
    this.cleanupRunning = value;
  }

  /**
   * This is the actual work method that runs a cleanup.
   * 
   * It builds a list of items (the "deadpool") that must be removed then removes them.
   * 
   * The list is built by scanning every item in the cache and deciding
   * what should happen with it. The decision is handled in the Deadpool.add()
   * method.
   * 
   * In between inspecting all cached items we keep an eye out for reaching
   * one of the three terminating conditions as explained in the comment header
   * for this class.
   */
  protected void cleanupWork() {

    // We keep track of the number of times this method was called.
    this.cleanupCount.incrementAndGet();
    
    LOG.info("{} cleanup: starting cleanup run targeting {} items for removal from the cache ", getTag(), removePerCleanup);
    
    // This timestamp in NANO seconds is used to measure the duration of the cleanup process as we progress.
    long startNanos = System.nanoTime();
    
    // When this time in NANO seconds is reached we must forcibly stop scanning because the allowed time
    // has been consumed.
    long stopNanos = startNanos + (this.maxCleanupDuration * 1000);

    // This is the collection of items that will be removed. It is a list that allows for some neat tricks that we need.
    Deadpool<T> deadpool = new Deadpool<>( removePerCleanup, this.getMaxAge());
    
    // This is the number of items in the cache that we have already looked at in this run
    int inspected = 0;

    // The reason we stopped scanning is used in the autotuning at the end of this run.
    ReasonTerminated reasonScanStopped = ReasonTerminated.ScanCompleted;
    
    // Here is the actual scan that inspects all data items in the cache
    for (Map.Entry<String, T> entry : getData().entrySet()) {
      
      // For reporting we keep track of the number of items that we have scanned
      inspected++;
      
      // The deadpool decides what to do with this cached item. We pass it the complete entry
      // because we need to keep track of the key as well.
      deadpool.add(entry);
      
      // Determine possible stop conditions.
      if (deadpool.isAllExpiredFound()) {
        // All items in the deadpool are expired
        reasonScanStopped = ReasonTerminated.AllExpired;
        break;
      }
      if (stopNanos < System.nanoTime()) {
        // We have run out of time
        reasonScanStopped = ReasonTerminated.Aborted;
        break;
      }
    }
    
    LOG.debug(
        "{} cleanup: Scan termination= {}. {} items were collected for deletion. {} items were inspected in {} microseconds",
        getTag(), reasonScanStopped, deadpool.getExpiredSize(), inspected, ((System.nanoTime() - startNanos) / 1000));

    // For statistics we keep track of the average age of the deleted items
    int sumAge = deadpool.sumExpiredAges() + deadpool.sumUnexpiredAges();
    
    
    // Finally remove all items that were collected in the deadpool from the cache.
    // NOTE: calling the keys from the deadpool changes the deadpools data internally.
    String key = deadpool.getNextKillKey();
    int removed = 0;
    
    while( key != null) {
      remove( key);
      removed++;
      key = deadpool.getNextKillKey();
    }
    
    // Avoid division by zero in our report
    String avgAgeString = "(not calculated)";
    avgCleanupAge  = -1;
    if( removed > 0) {
      avgCleanupAge  = (sumAge / removed) / 1000;
      avgAgeString = Integer.toString( avgCleanupAge);
    }
    
    // Now report what we have done
    // This is the time it took in miscroseconds
    lastCleanupDuration = (System.nanoTime() - startNanos) / 1000;

    // Here is an INFO report
    LOG.info(
        "{} cleanup: duration in microseconds={} Cache size after cleanup is {}. {} items were deleted with an average age of {}",
        getTag(), lastCleanupDuration, getSize(), removed, avgAgeString);

    // Finally, autotune the cleanup parameters for optimal performance in the next cleanup round.
    autotune( this.removePerCleanup, reasonScanStopped);
  }

  public int getMaxCleanupDuration() {
    return maxCleanupDuration;
  }

  public void setMaxCleanupDuration(int maxValue) {
    if( maxValue > 100) {
      this.maxCleanupDuration = maxValue;
      return;
    }
    throw new InvalidParameterException( "maxCleanupDuration must be at least 100 microseconds, attempt to set it to " 
    + maxValue);
  }

  public int getMaxRemovePerCleanup() {
    return maxRemovePerCleanup;
  }

  public void setMaxRemovePerCleanup(int maxValue) {
    if( (maxValue < 3) || ( maxValue > (getMaxSize() / 2) )) {
      throw new InvalidParameterException( "maxRemovePerCleanup must be at least 3 and at most " + (getMaxSize() / 2) + ". An attempt was made to set it to " 
          + maxValue);
    }
    this.maxRemovePerCleanup = maxValue;
  }

  public int getRemovePerCleanup() {
    return removePerCleanup;
  }

  public void setRemovePerCleanup(int value) {
    if( ( value < 3) || ( value > getMaxRemovePerCleanup())) {
      throw new InvalidParameterException( "removePerCleanup must be at least 3 and at most " + getMaxRemovePerCleanup() + ". An attempt was made to set it to " 
          + value);
    }
    this.removePerCleanup = value;
  }
  
  public int getCleanupCount() {
    return this.cleanupCount.get();
  }
  
  @Override
  public void resetCount() {
    this.cleanupCount.set(0);
    super.resetCount();
  }

  public long getLastCleanupDuration() {
    return lastCleanupDuration;
  }

  public int getAvgCleanupAge() {
    return avgCleanupAge;
  }
}
