package nl.dimario.cache;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This class collects items that must be removed from the cache. It has two lists,
 * one for expired items and one for unexpired items.
 * Both lists have a predetermined size. The expired list will contain
 * ideally as much expired items as the number that the cleanup run 
 * is supposed to remove from the cache. But it is possible that
 * we can't find enough expired items to satisfy this number.
 * 
 * The unexpired list contains unexpired items that will be deleted
 * in addition to the expired items in the other list, but only if
 * this is necessary to satisfy the number of items that must be deleted.
 * 
 * As the number of expired items grows, the number of items in the
 * unexpired list is allowed to shrink because finding more expired 
 * items means that we need to delete less unexpired items.
 * 
 * Items that are not yet expired are only placed on the list if they
 * have been unused for longer than one of the other unexpired items
 * that is already in the list. As we encounter more and more unexpired 
 * items in the cache the ones with longer "unused" time will replace
 * the items with a shorter unused time in the unexpired list.
 * 
 * In the end, the items in the unexpired list are the ones that have
 * gone unused by the application for the longest time and therefore
 * may be deleted from the cache. And also, in the end the list of
 * unexpired items will contain just enough items so that together
 * with the expired items on the other list we have the required number
 * of items that this cleanup run is supposed to remove.
 * 
 * This deadpool does not concern itself with running out of the
 * allotted time for scanning, that is done by the cache itself.
 * 
 * But we can tell here when the condition is reached where
 * enough expired items have been found to satisfy the required 
 * number for this cleanup run.
 * 
 * After the cache has determined that the scanning process is finished,
 * it will use the keys contained in the two lists of the deadpool
 * to actually remove the moribund items from the cache.
 * For this purpose a convenience method exists which will roll both 
 * lists into one before returning the first key.
 * 
 * @author Mario Pinkster
 *
 */
public class Deadpool<T extends CachedItem> {
  
/**
 * This list will be gradually filled up with expired items.
 */
  private List<Map.Entry<String, T>> expiredItems;
  
  /**
   * This list will be gradually filled up with unexpired items
   * which have gone unused for the longest of time.
   */
  private List<Map.Entry<String, T>> longestUnusedItems;
  
  /**
   * We are looking for this number of items to delete
   */
  private int requiredSize;
  
  /**
   * The kill index is the index into our merged list of items to be deleted.
   * It is used when reporting back key values for which to remove items from the cache.
   * We set it initially to -1 to signal that some initialization must take place. 
   */
  private int killIndex = -1;
  
  /**
   * Items with an age past this number are considered to be expired in the cache
   */
  private int maxAge;
  
  /**
   * Constructor takes some settings from the cache that creates us and
   * initializes list structures.
   */
  public Deadpool( int requiredSize, int maxAge) {
    this.requiredSize = requiredSize;
    this.maxAge = maxAge;
    this.expiredItems = new ArrayList< Map.Entry<String, T>>( requiredSize);
    this.longestUnusedItems = new ArrayList< Map.Entry<String, T>>( requiredSize);
  }
  
  /**
   * Adding an entry from the cache to our deadpool may result in the
   * item and its key being added to the list of expired items, or to the
   * list of unexpired items, or for it to be ignored.
   */
  public void add( Map.Entry<String, T> entry) {

    // Sanity check: if we are already returning keys we cannot add more items.
    if( killIndex > -1) {
      throw new IllegalStateException( "Cannot add more items in Deadpool when already started returning keys");
    }
    
    // Get the item and determine if it is expired.
    T item = entry.getValue();
    boolean expired = (item.getAge() > this.maxAge);
    if( expired) {
      // Add this entry to the "expired" list
      expiredItems.add(entry);
      return;
    }
    
    // If this is the very first item that we see which is not expired,
    // just add it to the list in order to initialize the list.
    if( longestUnusedItems.size() < 1) {
      longestUnusedItems.add(entry);
      return;
    }
    
    // The item is not expired. Determine how long it was unused and then see if
    // it must possibly replace one of the items already in the list of unexpired items.
    int currentUnused = item.getUnused();
    
    // Also determine how many unexpired items we need, this shrinks
    // with the number of expireds we have already found.
    int requiredUnexpired = this.requiredSize - expiredItems.size();
    
    // Walk the list of unexpired items in sequential order from the start.
    // If we find an item that has a shorter unused value we must insert
    // the currently investigated entry into the list while bumping all
    // entries with lower unused time towards the end of the list and discarding
    // the ones at the end if the list was already filled to capacity.
    int index = 0;
    while( index < longestUnusedItems.size()) {
      
      Map.Entry<String, T> test = longestUnusedItems.get( index);
      int testUnused = test.getValue().getUnused();
      if( testUnused < currentUnused) {
        // Must insert this entry and shift other entries up
        longestUnusedItems.add(index, entry);
        
        // And truncate the list.
        int truncateAt = requiredUnexpired;
        
        // But make sure we don't truncate before the beginning of the list
        if( truncateAt < 1) {
          truncateAt = 1;
        }
        // Truncate the list by removing the unwanted items at the end of the list.
        int i = longestUnusedItems.size() - 1;
        while( i >= truncateAt) {
          longestUnusedItems.remove(i);
          i--;
        }
        return;
      }
      index++;
    }
    // If we have not collected the required number of unexpireds, add this one at the end
    // of the list
    if( longestUnusedItems.size() < requiredUnexpired) {
      longestUnusedItems.add(entry);
    }
  }
  
  /**
   * Determine whether the list of expired items has the required size.
   * If so, we can stop scanning.
   */
  public boolean isAllExpiredFound() {
    return( this.expiredItems.size() >= this.requiredSize);
  }
  
  /**
   * Return the keys for the items that we have found that must be deleted.
   * At the start we merge the unexpired items with the expired ones.
   * Then for each consecutive call we return the next key until we
   * have exhausted all keys at which point we will return null.
   */
  public String getNextKillKey() {

    if( killIndex < 0) {
      // must merge lists
      expiredItems.addAll(longestUnusedItems);
    }
    killIndex++;
    if( killIndex >= requiredSize) {
      // We have exhausted all elements to kill
      return null;
    }
    // return the key for this entry on the kill list
    return expiredItems.get( killIndex).getKey();
  }
  
  /**
   * @return the number of expired items in the expired item list
   */
  public int getExpiredSize() {
    return expiredItems.size();
  }
  
  /**
   * @return the sum of the ages of all items in the given list.
   */
  private int sumItemAges( List<Map.Entry<String, T>> list) {
    int sum = 0;
    for( Map.Entry<String, T> entry : list) {
      sum += entry.getValue().getAge();
    }
    return sum;
  }
  
  public int sumExpiredAges() {
    return sumItemAges( this.expiredItems);
  }
  public int sumUnexpiredAges() {
    return sumItemAges( this.longestUnusedItems);
  }
}
