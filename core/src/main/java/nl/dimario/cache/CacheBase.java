package nl.dimario.cache;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This simple cache is based on HashMap and thread safe. Its settings (including size and time to
 * live) can be changed at runtime via a web interface.
 * 
 * Cleaning up stale cached items happens in the context of a put() call. There is no separate
 * daemon thread that scans the cache periodically to weed out stale ietems.
 * 
 * A call to put() an object in the cache with a new key checks if this insertion would violate the
 * max allowed size and runs a cleanup if necessary.
 * 
 * When a cleanup run is in progress a flag is set that prevents other threads from putting items in
 * the cache. This happens in a non-blocking way; the put() fails silently.
 * 
 * The get() operation is not influenced by this cleanup flag. But when the cleanup run actually
 * removes an item from the cache, normal Java synchronization is applied.
 * 
 * The put() operation also uses normal Java synchronization when adding a new item.
 * 
 * The actual algorithm that determines which items are to be removed during a cleanup run is not
 * coded in this base class but should be provided by implementations.
 * 
 * Items that can be cached by implementations of this abstract base class must all extend
 * CachedItem. It has attributes and methods for keeping track of the life cycle of a cached item.
 * 
 * @author Mario Pinkster
 */

/**
 * The Type T is the type of items that will be cached by actual instances of implementations of
 * this abstract base class.
 */
public abstract class CacheBase<T extends CachedItem> {

  private static Logger LOG = LoggerFactory.getLogger(CacheBase.class);

  /**
   * This map holds the items that are cached. Insertions and deletions are made threadsafe by
   * synchronizing the operations against this map object. The keys that identify items in the cache
   * are of type String.
   */
  private Map<String, T> data;

  /**
   * This tag is used to identify one cache instance both in the logging and in the web interface.
   * Uniqueness is not enforced by the cache; it is the responsebility of the application programmer
   * to ensure that each cache has a unique identification.
   */
  private String tag;

  /**
   * This is the current number of cached items. It is updated for each insertion or removal.
   */
  private AtomicInteger size;

  /**
   * This is the maximum number of items that may be cached. This value is provided in the
   * constrctor but can be changed at runtime.
   * 
   * When the maxSize is reached an attempt to add an additional item will trigger a call to the
   * cleanup() method (which is provided by the implementation of this abstract base class).
   */
  private int maxSize;

  /**
   * This is the maximum age that items in this cache can reach before they expire. The value is
   * provided initially to the constructor but can be changed at run time.
   * 
   * The getter and setter and the constructor use the Second as dimension for this value, however
   * internally we use milliseconds to represent this value.
   * 
   * All items in one instance of the cache share the same maxAge value.
   */
  private int maxAge;

  /**
   * These counters are used for obtaining statistics about the efficiency of the cache. They are of
   * type AtomicInteger for automatic thread safeness.
   */

  // Number of times the get() method is called.
  private AtomicInteger getCount;

  // Number of times the get() method found a non expired item in the cache (success).
  private AtomicInteger hitCount;

  // Number of times the get() method did not find an item in the cache.
  private AtomicInteger missCount;

  // Number of times the get() method found an expired item in the cache.
  private AtomicInteger expiredCount;

  // At this time the next round of statistics will be logged.
  private long nextReport;

  /**
   * This map is a registry of all known instances of implementations of CacheBase at runtime.
   * Creation of a new cache automatically registers it.
   */
  private static Map<String, CacheBase<? extends CachedItem>> registry =
      new HashMap<String, CacheBase<? extends CachedItem>>();

  /**
   * Implementations of this abstract base class must provide actual coding for the cleanup process.
   * The intended use of clenaup() is that it will remove obsolete items from the cache in order to
   * create space to cache new, fresh items.
   * 
   * What actual strategy or algorithm is used to determine which items are obsolete and can be
   * removed is the subject of a interesting field of study.
   * 
   * For the time being I will implement only one single agorithm, a variation of the Least Recently
   * Used algorithm that will remove multiple stale items from the cache during a single cleanup
   * run. The number of removed stale items can vary and is tuned automatically at the end of each
   * cleanup run.
   * 
   * All information we need for this algorithm to work is contained in the attributes and methods
   * of the CachedItem base class.
   */
  abstract void cleanup();

  /**
   * This flag is manipulated by the cleanup process in order to signal that a cleanup is currently
   * in progress. This in turn causes the put() method to fail graciously without blocking other
   * threads. The idea behind this behaviour is to prevent concurrent modifications to the Map that
   * is the actual cache structure when different threads are interacting with the cache.
   */
  abstract boolean isCleaningUp();

  /**
   * The initializer performs sanity checks on the values and may throw exceptions.
   * 
   * @param tag - an identifier. The application programmer must ensure uniqueness.
   * @param maxSize - the capacity in number of cached items. Values below 10 are rejected.
   * @param maxAgeSeconds - the maximum time IN SECONDS that items will be considered valid data.
   *        Values below 3 are rejected.
   * @throws InvalidParameterException when cache parameters are nonsensical.
   */
  protected void init(String tag, int maxSize, int maxAgeSeconds) {

    this.tag = checkTag(tag);
    this.setMaxSize(maxSize);

    // This setter converts the user supplied value to milliseconds for internal use.
    this.setMaxAge(maxAgeSeconds);
    this.data = new HashMap<String, T>(maxSize + 2);
    this.setSize(0);;

    // Register this new instance in the list of caches.
    synchronized (registry) {
      registry.put(tag, this);
    }
  }

  protected CacheBase() {

    // This timestamp controls when a statistic report is logged at DEBUG level (every 30 seconds).
    this.nextReport = System.currentTimeMillis();

    // Initiaize the threadsafe size and counter attributes
    this.size = new AtomicInteger(0);
    this.getCount = new AtomicInteger(0);
    this.hitCount = new AtomicInteger(0);
    this.missCount = new AtomicInteger(0);
    this.expiredCount = new AtomicInteger(0);
  }

  /**
   * Check if the tag name is acceptable. It may not be an empty string, null, or duplicate of a
   * tagname for a cache already registered.
   * 
   * @return trimmed value of the input.
   */
  private static String checkTag(String testTag) throws InvalidParameterException {

    boolean OK = (testTag != null);
    if (OK) {
      testTag = testTag.trim();
      OK = testTag.length() > 0;
    }
    if (OK) {
      synchronized (registry) {
        if (registry.containsKey(testTag)) {
          OK = false;
        }
      }
    }
    if (!OK) {
      throw new InvalidParameterException("The tag \"" + testTag + "\"is null, empty or duplicate");
    }
    return testTag;
  }

  /**
   * get() looks up the item (cached object) for the given key. If it is not there, the result is
   * null. If it is found, the item is inspected to see if it is still viable (max age not
   * exceeded). If all is good, the object is returned. If it was found stale, it is removed from
   * the cache and null is returned. During all this happening the statistics counters are updated
   * according to what is found (or not).
   * 
   * @param key - the identification for the object we want to find in the cache.
   * @return the found object or null if not found or found but expired.
   */
  public T get(String key) {

    // Update statistics
    getCount.incrementAndGet();

    // Check if we want to log some statistics to the info log.
    if (System.currentTimeMillis() > nextReport) {

      // Update the timestamp for the next occurence of statistic logging
      this.nextReport = System.currentTimeMillis() + 30000;

      // Here is the statistics report.
      LOG.info("{} statistics: {} / {} items, {} total gets, {} hits, {} misses, {} expired", tag,
          getSize(), getMaxSize(), getCount, hitCount, missCount, expiredCount);
    }

    // See if we can find the item
    T item = data.get(key);
    if (item == null) {
      // Update statistics, return "not found"
      missCount.incrementAndGet();
      return null;
    }
    // See is the item is expired
    long age = item.getAge();
    if (age > this.getMaxAge()) {
      // It is. Remove it from the cache, upate statitics and return "not found".
      remove(key);
      expiredCount.incrementAndGet();
      return null;
    }
    // Update statistics, update the "last used" on the item itself and return the found item
    hitCount.incrementAndGet();
    item.update();
    return item;
  }

  /**
   * Remove the item for the given key. If the item was not in the cache this is logged but triggers
   * no application error.
   * 
   * @param key - the key for which to remove the item.
   */
  protected void remove(String key) {

    CachedItem item = null;
    synchronized (data) {
      // Catch the result of the removal for logging purposes
      item = data.remove(key);
      size.decrementAndGet();
    }
    if (item == null) {
      LOG.debug("{} remove: item to remove was not present in cache for key=\"{}\"", tag, key);
    } else {
      LOG.debug("{} remove: item with key=\"{}\" and age {} was removed from cache", tag, key,
          item.getAge());
    }
  }

  /**
   * Remove one item for the given key while checking input sanity.
   * 
   * @param key - the key for which to remove the item.
   */
  public void removeOne(String key) {

    // This is an API call and therefore we perform a sanity check.
    if (key != null && key.length() > 0) {
      LOG.info("{} removeOne: removing item for key={}", tag, key);
      remove(key);
    } else {
      LOG.info("{} removeOne: called with empty or null key", tag);
    }
  }

  /**
   * put() checks if there is enough space left and calls clenaup() when not.
   * 
   * @param key - the identification key for the new item
   * @param item - the item (object of type T) that must be stored in the Map.
   */
  public void put(String key, T item) {

    // The cleanup() process may conceivably throw exceptions so we need to handle those.
    try {
      // If the cleanup is running we must fail graciously.
      if (isCleaningUp()) {
        LOG.warn(
            "{} put for key {} was ignored due to cleanup running. There are {} items present in the cache",
            tag, key, size);
        return;
      }
      // No cleanup running, we may proceed to add this item. First set its timestamps.
      item.init();

      // Do we have enough space left?
      if (getSize() >= getMaxSize()) {
        // No. Must call cleanup()
        cleanup();
      }
      // Add the new item to the Map using standard Java object locking for thread safety.
      synchronized (data) {
        data.put(key, item);
        size.incrementAndGet();
      }
      LOG.debug("{} put: item added for key=\"{}\". There are {} items in the cache", tag, key,
          size.get());

    } catch (RuntimeException x) {
      LOG.error("{} put: {}", tag, x.getMessage());
      throw x;
    }
  }

  /**
   * Remove all items from the Map.
   */
  synchronized public void clearAll() {

    LOG.info("{} clear all: removing all items from this cache", getTag());
    data.clear();
    size.set(0);
    resetCount();
  }

  /**
   * This is a sorted list of all known instances of the CacheBase implementations that are
   * currently present in this application.
   */
  synchronized public static List<CacheBase<? extends CachedItem>> getCacheList() {

    ArrayList<CacheBase<? extends CachedItem>> list =
        new ArrayList<CacheBase<? extends CachedItem>>(registry.values());
    
    list.sort(new Comparator<CacheBase<? extends CachedItem>>() {
      @Override
      public int compare(CacheBase<? extends CachedItem> o1, CacheBase<? extends CachedItem> o2) {
        return o1.getTag().compareTo(o2.getTag());
      }
    });
    return list;
  }

  /**
   * This clear method is intended for use with JUnit tests only, you won't need to call it
   * during normal operation of your application.
   */
  synchronized protected static void clearCacheRegistry() {
    registry.clear();
  }
  
  /**
   * This is a convenience method that lets you find a cache in the registry by its tag.
   * Note that when used for an actual implementation of CacheBase you'll need to cast
   * the result to your own cache type.
   * An alternative is to keep track of all the caches you create in your application
   * yourself by assigning them to named globals and managing these.
   */
  synchronized public static CacheBase<? extends CachedItem> getCache( String tag) {
    return registry.get(tag);
  }
  
  /**
   * The Map with cached items must be made available to child classes in order for them to
   * implement the cleanup() method. Derived classes should not use the data map directly to put()
   * or get() data items for other purposes.
   */
  protected Map<String, T> getData() {
    return this.data;
  }

  /**
   * Reset statistics counters (used for the web interface).
   */
  public void resetCount() {

    this.expiredCount.set(0);
    this.getCount.set(0);
    this.hitCount.set(0);
    this.missCount.set(0);
    this.nextReport = System.currentTimeMillis();
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  synchronized public int getMaxAge() {
    return maxAge;
  }

  /**
   * To the outside world, max age for items in the cache is represented in seconds. However,
   * internally we keep track of it in milliseconds.
   * 
   * @throws InvalidParameterException when the given value is below 3 seconds.
   */
  synchronized public void setMaxAge(int maxAgeValue) {
    if (maxAgeValue > 2) {
      this.maxAge = maxAgeValue * 1000;
      return;
    }
    throw new InvalidParameterException(
        "Max Age must be three seconds or more, attempt to set it to " + maxAgeValue);
  }

  synchronized public int getMaxSize() {
    return maxSize;
  }

  /**
   * Setting Max Size checks for sensible values.
   * 
   * @throws InvalidParameterException when the given value is below 10 items.
   */
  synchronized public void setMaxSize(int maxSizeValue) {
    if (maxSizeValue > 9) {
      this.maxSize = maxSizeValue;
      return;
    }
    throw new InvalidParameterException(
        "Max Cache Size must be ten or more, attempt to set it to " + maxSizeValue);
  }

  synchronized public int getSize() {
    return size.get();
  }

  synchronized private void setSize(int newSize) {
    this.size.set(newSize);
  }

  public int getGetCount() {
    return getCount.get();
  }

  public int getHitCount() {
    return hitCount.get();
  }

  public int getMissCount() {
    return missCount.get();
  }

  public int getExpiredCount() {
    return expiredCount.get();
  }
}

