package nl.dimario.cache;

/**
 * This class is the base for all object types that your application
 * wants to cache in an instance of LRUCache.
 * 
 * Because of this intermediate layer your cached data instances do
 * not need a separate housekeeping object for every item in the cache.
 * But for this reason the caching system is not versatile, in the
 * sense that your cached objects *must* extend this base class.
 * 
 * NOTE: the functions that return an int in this class arrive at
 * the value by subtracting two long values and casting this to int.
 * In theory this can lead to overflow as the difference of two longs
 * can be larger than fits inside an int. 
 * But because all long values are obtained from System.currentTimeMillis()
 * it is anticipated that the actual differences will not cause
 * problems as they will be time intervals (expressed in milliseconds)
 * which at most will span one complete lifecycle of your application.
 * 
 * @author Mario Pinkster
 */
public class CachedItem {

  /**
   * This long is a timestamp that marks the precise time that the
   * item was put into the cache. Ideally it should be a final but
   * finals can only be set in the constructor which is undesirable
   * because then all cached data types would have to call their
   * super() inside of their own constructor.
   * 
   * The value of start is used to determine the age of an item
   * at the present time.
   */
  private long start;

  /**
   * init() sets the value of both start and used to the present.
   * It is called by put() when a new item is placed
   * into the cache.
   */
  public void init() {
    long start =  System.currentTimeMillis();
    this.start = start;
    this.used = start;
  }
  
  /**
   * @return the time since this item was first placed in the
   * cache (in milliseconds). This is called the "age".
   */
  public int getAge() {
    long now = System.currentTimeMillis();
    return (int) (now - this.start); // as noted, subracting longs and casting to int
  }
  
  /**
   * This timestamp records the last time that an item was retrieved
   * from the cache using the get() method. Initially it
   * is set to the same value as this.start, then each
   * call to get() updates this value.
   * 
   * getUnused() uses this value to determine how long in the past this
   * item was retrieved from the cache at a given time. In other
   * words: how long has this item sat unused in the cache?
   * 
   */
  private long used;

  /**
   * update() is called from get() to update the 
   * "last used" timestamp to the present time.
   */
  public void update() {
    long now = System.currentTimeMillis();
    this.used = now;
  }

  /**
   * @return the number of milliseconds since this item was last 
   * retrieved from the cache by a get() operation.
   */
  public int getUnused() {
    long now = System.currentTimeMillis();
    return (int) (now - this.used);  // as noted, subracting longs and casting to int
  }
}
