package nl.dimario.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import java.util.Map;
import org.junit.Test;

/**
 * This class puts Deadpool through its paces.
 * The general working of a test is to present the add() method with a 
 * controlled succession of mock cache entries wrapped in a Map.Entry
 * and then test if we get the expected key values in the expected order. 
 * 
 * @author Mario Pinkster
 */

public class DeadpoolTest {
  
  /**
   * We want to control how the cached items behave with respect to the deadpool
   * so here is a mockup that returns the exact values that we want without looking
   * at System.currentTimeMillis()
   */
  
  protected class CachedItemMock extends CachedItem {
    
    private int age;
    private int unused;
    protected CachedItemMock(int age, int unused) {
      this.age = age;
      this.unused = unused;
    }
    
    @Override
    public int getAge() {
      return this.age;
    }
    
    @Override
    public int getUnused() {
      return this.unused;
    }
  }
  
  /**
   * create and return a mock cached item wrapped in a Map.Entry. 
   */
  private Map.Entry<String, CachedItemMock> createMockCacheEntry( final String key, int age, int unused) {
    
    final CachedItemMock mockItem = new CachedItemMock(age, unused);
    Map.Entry<String,CachedItemMock> mockEntry = new Map.Entry<String, CachedItemMock>(){

      @Override
      public String getKey() {
        return key;
      }

      @Override
      public CachedItemMock getValue() {
        return mockItem;
      }

      @Override
      public CachedItemMock setValue(CachedItemMock value) {
        throw new UnsupportedOperationException( "Mock class has no setter");
      }      
    };
    
    return mockEntry;
  }
  
  /**
   * fill a Deadpools data structure with a series of mock cache entries.
   * The input is an array of convenience data structures { key, age, unused }
   * where key, age and unused are used to make the mock item. 
   */
  class MockData {
    protected String key;
    protected int age;
    protected int unused;
    protected MockData( String key, int age, int unused) {
      this.key = key;
      this.age = age;
      this.unused = unused;
    }
  }
  
  private void fillPool( Deadpool<CachedItemMock> pool, MockData[] data) {
    
    for( int i=0; i<data.length; i++) {
      MockData md = data[i];
      Map.Entry<String, CachedItemMock> mi = createMockCacheEntry( md.key, md.age, md.unused);
      pool.add( mi);;
    }
  }
  
  /**
   * Check that the keys returned by the pool are as we expect, and that there
   * are no  more keys than what we expect.
   */
  private void checkKeys( Deadpool<CachedItemMock> pool, String[] expectedKeys) {
    
    String gotten;
    for( int i=0; i<expectedKeys.length; i++) {
      String expected = expectedKeys[i];
      gotten = pool.getNextKillKey();
      assertEquals( "Deadpool returns incorrect kill key", expected, gotten);
    }
    // See if we got precisely all the keys we expected and nothing more:
    gotten = pool.getNextKillKey();
    assertNull( "Deadpool returns too many kill keys", gotten);
  }

  @Test
  public void testAllExpired() {

    // This is the deadpool that we will be testing
    Deadpool<CachedItemMock> pool = new Deadpool<>( 5, 10);
    
    // The size is 5, maxAge is 10. present five expired cache items to the pool.
    // The value of "unused" should not matter but we vary it a bit anyway.
    MockData[] mockData = {
        new MockData( "C", 15, 2),
        new MockData( "A", 12, 22),
        new MockData( "D", 19, 2),
        new MockData( "F", 11, 11),
    };
    fillPool( pool, mockData);

    // Check reporting of "All filled up" for expired list after adding these four.
    assertFalse( "Deadpool.isAllExpiredFound reports incorrectly ", pool.isAllExpiredFound());

    // Adding the last expired element changes what isAllExpiredFound returns
    pool.add( createMockCacheEntry("B",  13,  13));
    assertTrue( "Deadpool.isAllExpiredFound reports incorrectly ", pool.isAllExpiredFound());   
    
    // Now check the keys that Deadpool reports. They should be
    // read back in the same order as the entries were added.
    checkKeys(pool,new String[] { "C", "A", "D", "F", "B"} );
  }
  
  @Test
  public void testAllUnExpiredExact() {

    // This is the deadpool that we will be testing
    Deadpool<CachedItemMock> pool = new Deadpool<>( 5, 30);
    
    // The size is 5, maxAge is 30. Present exactly five UNexpired cache items to the pool.
    // The value of "unused" matters because it determines the order in which keys are returned.
    MockData[] mockData = {
        new MockData( "C", 15, 5),
        new MockData( "A", 12, 22),
        new MockData( "D", 19, 2),
        new MockData( "F", 11, 11),
        new MockData( "B", 13, 1),
    };
    fillPool( pool, mockData);
    
    // Now check the keys that Deadpool reports. They should be
    // read back in order of "unused" descending.
    checkKeys(pool,new String[] { "A", "F", "C", "D", "B"} );
  }

  @Test
  public void testAllUnExpiredOneMore() {

    // This is the deadpool that we will be testing
    Deadpool<CachedItemMock> pool = new Deadpool<>( 5, 30);
    
    // The size is 5, maxAge is 30. Present SIX UNexpired cache items to the pool.
    // The value of "unused" matters because it determines the order in which keys are returned.
    MockData[] mockData = {
        new MockData( "C", 15, 5),
        new MockData( "E", 5,  4),
        new MockData( "H", 8,  7),
        new MockData( "D", 19, 2),
        new MockData( "B", 13, 1),
        new MockData( "A", 12, 22),
    };
    fillPool( pool, mockData);
    
    // Now check the keys that Deadpool reports. They should be
    // read back in order of "unused" descending.
    checkKeys(pool,new String[] { "A", "H", "C", "E", "D"} );
  }

  @Test
  public void testAllUnExpiredTwoMore() {

    // This is the deadpool that we will be testing
    Deadpool<CachedItemMock> pool = new Deadpool<>( 8, 30);
    
    // The size is 8, maxAge is 30. Present TEN UNexpired cache items to the pool.
    // The value of "unused" matters because it determines the order in which keys are returned.
    MockData[] mockData = {
        new MockData( "C", 15, 5),
        new MockData( "E", 5,  4),
        new MockData( "H", 25, 15),
        new MockData( "O", 7,  7),
        new MockData( "N", 10, 9),
        new MockData( "D", 19, 2),
        new MockData( "I", 8,  6),
        new MockData( "B", 13, 1),
        new MockData( "A", 12, 22),
        new MockData( "Z", 27, 25),
    };
    fillPool( pool, mockData);
    
    // Now check the keys that Deadpool reports. They should be
    // read back in order of "unused" descending. There should be
    // at most EIGHT keys returned due to the size of the pool.
    checkKeys(pool,new String[] { "Z", "A", "H", "N", "O", "I", "C", "E"} );
  }

  @Test
  public void testAllUnExpiredBiggestLast() {

    // This is the deadpool that we will be testing
    Deadpool<CachedItemMock> pool = new Deadpool<>( 5, 30);
    
    // The size is 5, maxAge is 30. Present fifteen UNexpired cache items to the pool,
    // starting with small values of unused then once the list of unexpireds is
    // partially filled adding one with a large expiration, then small ones again,
    // and finally a series of largest ones to see if they correctly replace the 
    // items with smaller unused values.
    // The value of "unused" matters because it determines the order in which keys are returned.
    MockData[] mockData = {
        new MockData( "A", 2,  1),
        new MockData( "B", 3,  2),
        new MockData( "C", 4,  3),
        new MockData( "D", 19, 19),
        new MockData( "E", 6,  5),
        new MockData( "F", 7,  6),
        new MockData( "G", 8,  7),
        new MockData( "H", 4,  2),
        new MockData( "I", 5,  3),
        new MockData( "J", 6,  6),
        new MockData( "K", 18, 18),
        new MockData( "L", 25, 25),
        new MockData( "M", 17, 17),
        new MockData( "N", 20, 20),
        new MockData( "O", 5,  1),
        new MockData( "P", 21, 21),
    };
    fillPool( pool, mockData);
    
    // Now check the keys that Deadpool reports. They should be
    // read back in order of "unused" descending.
    checkKeys(pool,new String[] { "L", "P", "N", "D", "K"} );
  }


  @Test
  public void testMixedPlusTwo() {

    // This is the deadpool that we will be testing
    Deadpool<CachedItemMock> pool = new Deadpool<>( 8, 20);
    
    // The size is 8, maxAge is 20. Present TEN mixed cache items to the pool.
    // Five will be expired, five not. nr 9 and 10  are unexpired and expired,
    // The value of "unused" in the unexpireds matters because it determines 
    // the order in which keys are returned after all expired keys were returned.    
    MockData[] mockData = {
        new MockData( "C", 15, 5),
        new MockData( "E", 34, 4),
        new MockData( "H", 25, 15),
        new MockData( "O", 7,  7),
        new MockData( "N", 22, 9),
        new MockData( "D", 21, 2),
        new MockData( "I", 8,  6),
        new MockData( "B", 13, 1),
        new MockData( "A", 12, 22),
        new MockData( "Z", 27, 25),
    };
    fillPool( pool, mockData);
    
    // Now check the keys that Deadpool reports.
    // First we get the five expired keys in order of addition,
    // then three unexpired keys in order of "unused" descending
    checkKeys(pool,new String[] { "E", "H", "N", "D", "Z", "A", "O", "I"} );
  }

  @SuppressWarnings("unused")
  @Test
  public void testIllegalState() {

    // This is the deadpool that we will be testing
    Deadpool<CachedItemMock> pool = new Deadpool<>( 8, 20);

    // We will add some entries, it doesn't matter which ones for this test,
    // then start getting keys and then add one more entry.
    // This must result in an exception.
    MockData[] mockData = {
        new MockData( "E", 6,  5),
        new MockData( "F", 7,  6),
        new MockData( "I", 5,  3),
        new MockData( "J", 6,  6),
        new MockData( "M", 17, 17),
        new MockData( "P", 21, 21),
    };
    fillPool( pool, mockData);
    
    String killKey = pool.getNextKillKey();
    killKey = pool.getNextKillKey();
    
    // Test to see if we get the exception what we expect with the correct text in the message
    try {      
      pool.add( createMockCacheEntry("ZZ", 11, 11));
      assertTrue( "Expected exception was NOT thrown", false);
      
    } catch( IllegalStateException xs) {
      String msg = xs.getMessage();
      assertTrue( "Wrong error message", msg.contains( "annot add more items"));
      
    } catch( Throwable t) {
      assertTrue( "Wrong type of exception was thrown", false);
    }
  }
}
