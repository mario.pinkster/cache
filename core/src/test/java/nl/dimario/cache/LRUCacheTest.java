package nl.dimario.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * @author Mario Pinkster
 */

public class LRUCacheTest {
  
  static LRUCacheMock create( String tag, int maxSize, int maxAgeSeconds) {
    LRUCacheMock result = new LRUCacheMock();
    result.init(tag, maxSize, maxAgeSeconds);
    return result;
  }

  @Test
  public void testAutotuneParams() {
    
    LRUCache<? extends CachedItem> kolo = LRUCache.create("kolo", 200, 60);
    
    int maxduration = kolo.getMaxCleanupDuration();
    assertEquals( "Max Duration is initialized to an unexpected value", 1750, maxduration);
    
    int maxpercleanup = kolo.getMaxRemovePerCleanup();
    assertEquals( "Max Cleanup count is initialized to an incorrect value", 20, maxpercleanup);
    
    int nextcleanupcount = kolo.getRemovePerCleanup();
    assertEquals( "Cleanup count is initialized to an incorrect value", 5, nextcleanupcount);
    
  }
  
  @Test
  public void testRecalcAutotuneDefaults() {
    
    LRUCache<? extends CachedItem> kilo = LRUCache.create("kilo", 200, 60);
    
    kilo.setMaxSize( 470);
    assertEquals( "Resize sets incorrect new size", 470, kilo.getMaxSize());
    
    
    int maxpercleanup = kilo.getMaxRemovePerCleanup();
    assertEquals( "Max Cleanup count is reinitialized incorrectly", 46, maxpercleanup);
    
    int nextcleanupcount = kilo.getRemovePerCleanup();
    assertEquals( "Cleanup count is reinitialized incorrectly", 11, nextcleanupcount);
  }
  
  @Test
  public void testAutotuneDefaultsBoundaries() {
    
    LRUCache<? extends CachedItem> cache = LRUCache.create("kali", 470, 60);
    try {
      cache.setMaxRemovePerCleanup( 101);
      cache.setRemovePerCleanup( 1);
      assertTrue( "expected exception was not thrown", false);
    } catch( Exception x) {
      String msg = x.getMessage();
      assertTrue( "incorrect error message", msg.contains( "at least 3"));
      assertTrue( "incorrect error message", msg.contains( "at most 101"));
    }
    try {
      cache.setMaxRemovePerCleanup( 101);
      cache.setRemovePerCleanup( 102);
      assertTrue( "expected exception was not thrown", false);
    } catch( Exception x) {
      String msg = x.getMessage();
      assertTrue( "incorrect error message", msg.contains( "at least 3"));
      assertTrue( "incorrect error message", msg.contains( "at most 101"));
    }
    try {
      cache.setMaxRemovePerCleanup( 2);
      assertTrue( "expected exception was not thrown", false);
    } catch( Exception x) {
      String msg = x.getMessage();
      assertTrue( "incorrect error message", msg.contains( "at least 3"));
      assertTrue( "incorrect error message", msg.contains( "at most 235"));
    }
    try {
      cache.setMaxRemovePerCleanup( 300);
      assertTrue( "expected exception was not thrown", false);
    } catch( Exception x) {
      String msg = x.getMessage();
      assertTrue( "incorrect error message", msg.contains( "at least 3"));
      assertTrue( "incorrect error message", msg.contains( "at most 235"));
    }
    try {
      cache.setMaxCleanupDuration( 50);
      assertTrue( "expected exception was not thrown", false);
    } catch( Exception x) {
      String msg = x.getMessage();
      assertTrue( "incorrect error message", msg.contains( "at least 100"));
    }
  }

  
  @Test
  public void testCleanupFlag() {

    LRUCacheMock mock = LRUCacheTest.create("mock", 60, 60);

    // Add some items to the cache or the cleanup won't start.
    CachedItem a = new CachedItem();
    CachedItem b = new CachedItem();
    CachedItem c = new CachedItem();
    
    mock.put("A",  a);
    mock.put("B",  b);
    mock.put("C",  c);
    
    // First without exceptions
    mock.cleanup();
    assertFalse( "\"Cleanup in progress\" flag was not cleared at normal cleanup termination", mock.isCleaningUp());
    
    // Then with
    mock.misbehave = true;

    try {
      mock.cleanup();
      assertTrue( "expected exception was not thrown", false);
    } catch( Exception x) {
      assertFalse( "\"Cleanup in progress\" flag was not cleared at cleanup termination with exception", mock.isCleaningUp());
    }
  }  
}

/**
 * This mock class is able to intentionally throw an exception from cleanupWork()
 * to see how exceptions are handled. 
 */
class LRUCacheMock extends LRUCache<CachedItem>  {
  
  boolean cleaningUp;
  boolean misbehave;
  
  @Override
  protected void cleanupWork() {
    if( misbehave) {
      throw new RuntimeException( "Misbehaving as requested");
    }
  }    
}
