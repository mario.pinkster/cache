package nl.dimario.cache;

/**
 * NOTE: The class CachedItem does not do very much and it has no separate
 * JUnit test of its own. Instead, its methods are tested here as a
 * part of other JUnit tests.
 * 
 * @author Mario Pinkster
 */
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.List;
import org.junit.Test;

@SuppressWarnings("unused")
public class CacheBaseTest {

  /**
   * Here is a concrete test implementation for the abstract CacheBase class.
   * It is used to test the methods in the abstract base class.
   * Also it explicitly overrides the cleanup flag so that we can manipulate it
   * for our tests.
   */
  
  static CacheBaseMock create( String tag, int maxSize, int maxAgeSeconds) {
    CacheBaseMock result = new CacheBaseMock();
    result.init(tag, maxSize, maxAgeSeconds);
    return result;
  }
  

  @Test
  public void testConstruction() {
    
    // Create and register a new Mock cache using a unique name tag.
    CacheBaseMock tesd = create("tesd", 30, 120);
    
    // Get the list of all registered implementations of CacheBase.
    // Note other tests could have added instances to this list.
    List<CacheBase<? extends CachedItem>> list = CacheBase.getCacheList();
    
    // This is the implementation that we will be testing (if we find it)
    CacheBase<? extends CachedItem> test = null;
    
    // Inspect all registered implementations of CachedBase looking for the one named "tesd"
    for( CacheBase<? extends CachedItem> item : list) {
      if( "tesd".equals(item.getTag())) {
        test = item;
      }
    }
    
    // Inspect what we have found and ascertain it has been initialized properly
    assertTrue( "New instance is not registered correctly", test != null);
    assertEquals( "tag is not properly set", "tesd", test.getTag());
    assertEquals( "max size is not properly initialized", 30, test.getMaxSize());
    assertEquals( "max age is not properly initialized", 120000, test.getMaxAge());
    
  }
  
  @Test
  public void testMinimumSize() {
    
    try {
      // Create and register a new Mock cache using a unique name tag.
      // MaxSize has an unacceptale value and the constructor should raise an exception.
      CacheBaseMock tetsuo = create("tetsuo", 5, 120);
      
      assertTrue( "Expected exception was not thrown", false);
    } catch( Exception x) {
      String msg = x.getMessage();
      assertTrue( "Incorrect error  message", msg.contains( "ten or more"));
      assertTrue( "Incorrect error  message", msg.contains( "set it to 5"));
    }
  }

  @Test
  public void testMinimumAge() {
    
    try {
      // Create and register a new implementation of CacheBase using a unique nametag.
      // MaxAge has an unacceptale value and the constructor should raise an exception.
      CacheBaseMock tortuga = create("tortuga", 20, 2);
      assertTrue( "Expected exception was not thrown", false);
    } catch( Exception x) {
      String msg = x.getMessage();
      assertTrue( "Incorrect error  message", msg.contains( "three seconds or more"));
      assertTrue( "Incorrect error  message", msg.contains( "set it to 2"));
    }
  }

  @Test 
  public void testPut() {
    
    // Create and register a new implementation of CacheBase using a unique nametag.
    CacheBaseMock taat = create("taat", 30, 120);

    // put a new cached item in this cache. The call to put() should
    // initialize the values concerned with the age of the item.
    CachedItem item = new CachedItem();
    taat.put("A", item);
    
    // Test the age, it should be close to zero milliseconds.
    int age = item.getAge();
    assertTrue( "start was  not initialized correctly", age <= 1);
    
    // Test the interval since this item was last used, this should be close to zero.
    int unused = item.getUnused();    
    assertTrue( "last used was  not initialized correctly", unused <= 1);
  }
  
  @Test 
  public void testPutAndGet() {
    
    // Create and register a new implementation of CacheBase using a unique nametag.
    CacheBaseMock tosti = create("tosti", 30, 120);

    // Put a new item in the cache, wait a bit and then retrieve it.
    CachedItem item = new CachedItem();
    tosti.put("B", item);
    try {
      Thread.sleep(10);
    } catch( InterruptedException x) {
      throw new RuntimeException( "Something wrong with Thread.sleep(): " + x.getMessage());
    }
    CachedItem test = tosti.get( "B");
    
    // Assert that we have found it:
    assertEquals( "Item was not found after a put()", item, test);
    
    // Note: due to timing irregularities in a multithreaded environment these tests
    // may fail occasionally because the getAge() and getUnused() methods evaluate the
    // differences with a point in the past against a freshly obtained current time.
    // This is most notable when you set breakpoints in this method for debugging.
    
    // Assert that it has aged properly:
    int age = test.getAge();
    assertTrue( "Age is incorrect after sleeping 10 ms", age < 12);
    
    // Assert last used was updated
    int unused = test.getUnused();
    assertTrue( "last used was  not updated by get()", unused <= 1);
  }
  
  @Test 
  public void testAbortedPutAndGet() {
    
    // Create and register a new implementation of CacheBase using a unique nametag.
    CacheBaseMock cache = create("tetscleanupflag", 30, 120);
    
    // Set the "I am running a cleanup" flag in the mock implementation
    cache.setCleaningUp(true);

    // Try to put a new item in the cache while the cleanup flag is raised.
    CachedItem item = new CachedItem();
    cache.put("B", item);
    
    // This get should NOT find the new item because the put() was aborted silently.
    CachedItem test = cache.get( "B");
    
    assertEquals( "An item was added with put() while the clenaup flag was raised", null, test);
  }
  
  @Test
  public void testRegistry() {
    
    // As we don't know the order in which the tests are run we must make this test method self-contained.
    // For that, we clear the registry and then add a known number of caches.    
    CacheBaseMock.clearCacheRegistry();
    
    // Add a known number of caches
    create("testregistry-A", 30, 120);
    create("testregistry-B", 30, 120);
    create("testregistry-D", 30, 120);
    
    List<CacheBase<? extends CachedItem>> registryList = CacheBase.getCacheList();    
    assertEquals( "Some instances of the cache are not registered correctly", 3, registryList.size());
  }
  
  // TODO: test size() and other counters and resetting them.
}

class CacheBaseMock extends CacheBase<CachedItem> {

  private boolean cleaningUp;
  

  @Override
  void cleanup() {
    return;
  }
  
  public void setCleaningUp( boolean value) {
    cleaningUp = value;
  }
  
  public boolean isCleaningUp() {
    return cleaningUp;
  }
}
