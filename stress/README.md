## Stress test

This project has some tooling to test the cache core and webinterface by making calls to the razz webserver 
application (which must be up and running for the test to work.

The main entry point for the actual stress test application is in Main.java. There, parameters are
initialized by reading a file the name if which is the sole command line arg for the app.
After initializing an Parameters instance, control is passed to RunTest to perform the actual
calls to the razz server.

Which parameters can be set and what they affect is explained in the source code for Parameters.java.

You will need a file containing key values to use for requesting stuff from the razz application.
A small utility to generate such files is in GenerateStressKeys.java. See the comments there on how to use it.

You can have multiple instances of this stress test application firing requests at the same razz application server,
either from different computers in your network or from the same computer. Each instance of the stress test
program can have its own set of parameters. 