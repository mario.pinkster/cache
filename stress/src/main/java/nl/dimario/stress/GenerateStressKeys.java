package nl.dimario.stress;

import java.io.File;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

/**
 * This class is a command line utility to generate a bunch of keys.
 * The output is a text file with one key per line. It is named
 * "keys" and sits in the current working directory.
 * There are no command line arguments for this program. The output is
 * always named "keys". The number of keys to generate is controlled by
 * the private static int in the classfile.
 * 
 * The file with generated keys is used by the stress client in
 * generating randomized requests to the razz webapplication,
 * while being able to consistently reproduce the same requests
 * in the same order for consecutive runs.
 */
public class GenerateStressKeys {
  
  // Edit this number to change the amount of key values to generate.
  private static int N = 174;
  
  public static void main( String[] argv)  {
    
    String startingChars = "AEIOUY";
    String continuingChars="0123456789BCFGKLPQVXZ";
    
    long startNanos = System.nanoTime();
    
    Set<String> keys = new HashSet<String>(N);
    while( keys.size() < N) {
      String key = RandomStressData.makeRandomKey( startingChars, continuingChars, 3);
      keys.add(key);
    }
    
    long generatedNanos = System.nanoTime();
    System.out.println( String.format("%d key generated in %d nanoseconds" ,  N, generatedNanos - startNanos));
    
    File output = new File( "./keys");
    try( PrintWriter pw = new PrintWriter( output)) {
      
      for( String key: keys) {
        pw.println( key);
      }
    } catch (Exception e) {
       e.printStackTrace();
    }
    System.out.println( String.format("%d keys written in %d nanoseconds" ,  N, System.nanoTime() - generatedNanos));
  }

}
