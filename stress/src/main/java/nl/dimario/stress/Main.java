package nl.dimario.stress;


/**
 * This standalone Java command line application is a HTPP client
 * that repeatedly sends requests to the razz webapplication.
 * The number of requests and the speed at which they are fired
 * (requests per second), and other settings are read at startup
 * from a parameter file.
 * 
 * The requests are specifically meant to interact with the razz webserver.
 * 
 * You can have multiple instances of this client running against the same
 * webserver at the same time (either from different computers or from
 * different terminals on the same computer).(Or both).
 * Each running instance can have its own set of parameters.
 */
public class Main {
  
  public static void main( String[] args) {
   
    String paramFile = "src/main/resources/razz.properties";
    if( args.length > 0) {
      paramFile = args[0];
    }
    Parameters parms = new Parameters();
    if( ! parms.initialize( paramFile)) {
      System.out.println( "Parameter file  \"" + paramFile + "\" not found.");
      System.exit( 1);
    }
    
    RunTest runTest = new RunTest();
    runTest.run(parms);
  }

}
