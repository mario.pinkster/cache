package nl.dimario.stress;

/**
 * This class implements the actual testing.
 * It creates random HTPP requests and fires them at the razz server
 * at random intervals in time but with a frequency determined in the 
 * test parameters.
 */

import java.net.URISyntaxException;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class RunTest {

  public void run( Parameters parms) {
    
    CloseableHttpClient httpClient = HttpClients.createDefault();
    
    long startTime = System.currentTimeMillis();
    
    long accuReqTime = 0;
    
    int fivePercent = parms.getNumberOfRuns() / 20;
    if( fivePercent  < 10) {
      fivePercent = 10;
    } else if( fivePercent > 50) {
      fivePercent = 50;
    }
      
        
    for( int i=0; i<parms.getNumberOfRuns(); i++) {
      try {
        
        HttpGet request = makeGET( parms);
//        System.out.println( "GET " + request.getURI());
        
        long startReq = System.nanoTime();
        CloseableHttpResponse response = httpClient.execute(request);
        long delta = System.nanoTime() - startReq;
        response.close();
        accuReqTime += delta;
        int wait = RandomStressData.makeRandomWaitMillis( parms.getMinWaitMillis(),  parms.getMaxWaitMillis());
        
        if( (i % fivePercent) == 1) {
          long averageRoundTrip = (accuReqTime / i) / 1000;
          float totalTime = (float) (System.currentTimeMillis() - startTime) / 1000;
          float requestsPerSecond = i / totalTime;
          System.out.println( String.format( "Run %6d: avg roundtrip=%d micros,  req/s=%2.2f", i,  averageRoundTrip, requestsPerSecond));
        }
        Thread.sleep(wait);
        
      } catch (Exception e) {
        e.printStackTrace();
        break;
      }
    }
  }
  
  private HttpGet makeGET( Parameters parms) throws URISyntaxException {
      URIBuilder uri = new URIBuilder();
      uri.setScheme( "http");
      uri.setHost( parms.getHost());
      uri.setPort( parms.getPort());
      uri.setPath( "/razz");
      uri.setParameter("command",  "load");
      String key = parms.getRandomKey();
      uri.setParameter("key",  key);
      String cachetag = parms.getRandomDataType();
      uri.setParameter("datatype", cachetag);
      return new HttpGet( uri.build());
  }
}
