package nl.dimario.stress;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;

/**
 * This class has all parameter values needed to control 
 * one instance of the client. The values are initialized
 * from a properties file.
 */
public class Parameters {

  /** parameter: The hostname and port to place the HTTP calls to. Defaults to "127.0.0.1:8080" */
  private String host;
  private int port;
  
  /** parameter: The name of a text file containing key values to use in the calls, one per line. Defaults to "keys" */
  private String keyFile;
  
  /** The number of key values in the keyFile, used for getting a random key */
  private int numberOfKeys;
  
  /** parameter: The minimum and maximum time in milliseconds to wait between two consecutive calls to the server. Defaults to 3,1111 */
  private int minWaitMillis;
  private int maxWaitMillis;
  
  /** parameter: How many requests to send before exiting the test. Defaults to 956 */
  private int numberOfRuns;
  
  /** parameter: Which dataTypes (which caches in the razz application) to make requests for (no default, is mandatory) */
  private String[] dataTypes;
  
  /** parameter: seed for the Random number generator. Defaults to 13659 */
  private int randomSeed;
  
  /** Two random number generators for key and type */
  private Random randomKey;
  private Random randomDataType;
  
  /** A list of all the keys in the keyFile */
  List<String> keyList;
  
  /**
   * The init() catches all exceptions from initialization 
   * and prints a stacktrace on error. 
   * 
   * @return true if succesful, false on error.
   */
  public boolean initialize( String paramFile) {
    File propFile = new File( paramFile);
    Properties properties = new Properties();
    try( InputStream is = new FileInputStream( propFile )) {
      
      properties.load(is);
      this.host = properties.getProperty("host", "127.0.0.1");
      this.port = Integer.parseInt( properties.getProperty("port", "8080"));
      this.keyFile = properties.getProperty( "keyFile", "keys" );
      this.minWaitMillis = Integer.parseInt( properties.getProperty("minWaitMillis", "3"));
      this.maxWaitMillis = Integer.parseInt( properties.getProperty("maxWaitMillis", "1111"));
      this.numberOfRuns = Integer.parseInt( properties.getProperty("numberOfRuns", "956"));
      this.randomSeed = Integer.parseInt( properties.getProperty( "randomSeed", "13659"));
      
      String dataTypeList = properties.getProperty( "dataTypes", "");
      dataTypes = dataTypeList.split(",");
      for( int i=0; i<dataTypes.length;i++) {
        dataTypes[i] = dataTypes[i].trim();
      }
      
      loadKeys();
      this.numberOfKeys = keyList.size();
      
      // Create random number generators to generate fixed sequences of keys and datatypes.
      this.randomKey = new Random( this.randomSeed);
      this.randomDataType = new Random( this.randomSeed + 112);
      
      report( paramFile);
      
      return true;
    } catch( Exception x) {
      x.printStackTrace();
    }
    return false;
  }

  private void report( String ParamFile) {
    
    System.out.println( "\n\n\nparameter file=" + ParamFile + "\n\n");
    System.out.println( "host="+getHost());
    System.out.println( "port="+getPort());
    System.out.println( "keyFile="+getKeyFile());
    System.out.println( "number of keys="+getNumberOfKeys());
    for( int i=0; i<dataTypes.length;i++) {
      System.out.printf( "dataType=%s\n", dataTypes[i]);
    }
    System.out.println( "minWaitMillis="+getMinWaitMillis());
    System.out.println( "maxWaitMillis="+getMaxWaitMillis());
    System.out.println( "numberOfRuns="+getNumberOfRuns());
    System.out.println( "randomSeed="+getRandomSeed());
    System.out.println( "\n\n\n");
  }
  
  
  private void loadKeys() throws IOException {
    
    long startNanos = System.nanoTime();
    
    Set<String> keys = new HashSet<String>();
    
    File input = new File( this.keyFile);
    try( BufferedReader br = new BufferedReader(  new FileReader( input))) {
   
      String key;
      while( (key = br.readLine()) != null) {
        if( key.startsWith( "#")) {
          continue;
        }
        key = key.trim();
        if( key.length() < 1) {
          continue;
        }
        keys.add( key);
      }
    }
    this.keyList = new ArrayList<String>( keys);
    System.out.println( String.format("%d key read in %d nanoseconds" ,  keys.size(), System.nanoTime() - startNanos));
  }
  
  public String getRandomKey() {
    int randomIndex = randomKey.nextInt( numberOfKeys);
    return keyList.get(randomIndex);
  }

  public String getRandomDataType() {
    int randomIndex = randomDataType.nextInt( dataTypes.length);
    return dataTypes[randomIndex];
  }
  
  public String getHost() {
    return host;
  }

  public int getPort() {
    return port;
  }

  public int getMinWaitMillis() {
    return minWaitMillis;
  }

  public int getMaxWaitMillis() {
    return maxWaitMillis;
  }

  public int getNumberOfRuns() {
    return numberOfRuns;
  }

  public String getKeyFile() {
    return keyFile;
  }
  
  public int getNumberOfKeys() {
    return numberOfKeys;
  }
  
  public int getRandomSeed() {
    return randomSeed;
  }
}
