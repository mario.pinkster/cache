package nl.dimario.stress;

import java.util.Random;
import nl.dimario.razz.RandomData;

public class RandomStressData {

  private static Random random;

  private static char randomChar(String chars) {

    int index = random.nextInt(chars.length());
    return chars.charAt(index);
  }

  public static String makeRandomKey(String startingChars, String continuingChars, int maxlen) {

    // Determine key length, minimum 1, maximum maxlen
    int len = RandomData.randomGaussian(((float) maxlen) / 2, ((float) maxlen - 1) / 2);
    if (len < 1) {
      len = 1;
    } else if (len > maxlen) {
      len = maxlen;
    }

    char[] raw = new char[len];
    raw[0] = randomChar(startingChars);
    for (int i = 1; i < len; i++) {
      raw[i] = randomChar(continuingChars);
    }

    return new String(raw);
  }

  public static int makeRandomWaitMillis(int minMillis, int maxMillis) {

    // Determine the wait until the next request is fired.
    int max = ((maxMillis > minMillis) ? maxMillis : minMillis);
    int min = ((maxMillis > minMillis) ? minMillis : maxMillis);
    int wait = RandomData.randomGaussian(((float) max + min) / 2, ((float) max - min) / 3);
    if (wait < minMillis) {
      wait = minMillis;
    } else if (wait > maxMillis) {
      wait = maxMillis;
    }
    return wait;
  }
}
