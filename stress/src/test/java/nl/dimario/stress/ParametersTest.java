package nl.dimario.stress;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class ParametersTest {

  @Test
  public void testLoadProperties() {
    Parameters parameters = new Parameters();
    parameters.initialize("src/test/resources/test.properties");

    assertEquals("host parameter bad", "localhost", parameters.getHost());
    assertEquals("port parameter bad", 8089, parameters.getPort());
    assertEquals("keyFile parameter bad", "src/test/resources/testkeys", parameters.getKeyFile());
    assertEquals("minWaitMillis parameter bad", 13, parameters.getMinWaitMillis());
    assertEquals("maxWaitMillis parameter bad", 233, parameters.getMaxWaitMillis());
    assertEquals("numberOfRuns parameter bad", 54, parameters.getNumberOfRuns());
    assertEquals("randomSeed parameter bad", 1337, parameters.getRandomSeed());
    assertEquals("incorrect number of keys read", 9, parameters.getNumberOfKeys());
  }

  @Test
  public void testRandomKeysAndDataTypes() {
    Parameters parameters = new Parameters();
    parameters.initialize("src/test/resources/test.properties");

    String key;
    String datatype;

    // Test some key and data type values somewhere in a sequence, they should not vary across runs
    for (int i = 0; i < 100; i++) {
      key = parameters.getRandomKey();
      datatype = parameters.getRandomDataType();
      if (i % 17 == 2) {
        System.out.printf("Random key after %d calls = %s, random data type = %s\n", i, key,
            datatype);
      }
      switch (i) {
        case 13:
          assertEquals("Unexpected value in random key sequence", "CC", key);
          assertEquals("Unexpected value in random data type sequence", "bicycle", datatype);
          break;
        case 51:
          assertEquals("Unexpected value in random key sequence", "BBB", key);
          assertEquals("Unexpected value in random data type sequence", "honesty", datatype);
          break;
        case 89:
          assertEquals("Unexpected value in random key sequence", "HH", key);
          assertEquals("Unexpected value in random data type sequence", "pear", datatype);
          break;
      }
    }
  }


}
