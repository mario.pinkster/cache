package nl.dimario.cache;

/**
 * This interfae defines all constants for the webinterface
 * 
 * @author Mario Pinkster
 */
public interface CacheAdminConst {
  
  enum Command {
      DISPLAY,
      SETPARAMS,
      RESETCOUNT,
      CLEANUPNOW,
      CLEARALL,
      SHOWKEYS,
      DELETEKEY,
      SHOWDOC
  };
  
  static final int    MAXFILTEREDKEYS = 150;
  
  static final String MESSAGE = "message";
  
  static final String CACHELIST = "cachelist";
  static final String CACHE = "cache";
  static final String TAG = "tag";
  static final String ACTION = "action";
  
  static final String SETPARAMS = "setparams";
  static final String MAXAGE = "maxage";
  static final String MAXSIZE = "maxsize";
  static final String MAXCLEANUP = "maxcleanup";
  static final String NEWCLEANUP = "newcleanup";
  static final String MAXDURATION = "maxduration";
  
  static final String RESETCOUNT = "resetcount";
  static final String CLEARALL = "clearall";
  static final String CLEANUPNOW = "cleanupnow";
  
  static final String KEYLIST = "keylist";
  static final String KEYPART = "keypart";
  static final String KEYTODELETE = "keytodelete";
  static final String WHAT = "what";
  static final String NUMBERS = "numbers";
  static final String FAQ = "faq";
}
