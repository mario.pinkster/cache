package nl.dimario.cache;

import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;

/**
 * This class has methods that perform various functions for the CacheAdminServlet.
 * 
 * All methods are static so we don't have to worry about an instance of this util class being
 * created at the correct time.
 * 
 * @author Mario Pinkster
 */
public class CacheAdminUtil implements CacheAdminConst {

  /**
   * Extract the action command from the request parameters. If it is not present or has an unknown
   * value, Command.Display is returned as a default value.
   */
  public static Command getAction(HttpServletRequest request) {

    // Default is display
    Command result = Command.DISPLAY;

    String action = request.getParameter(ACTION);
    if (action != null) {
      try {
        result = Command.valueOf(action.toUpperCase());
      } catch (IllegalArgumentException x) {
        result = Command.DISPLAY;
      }
    }
    return result;
  }

  /**
   * Find the cache that has the given tag. If no cache matches the given tag, return null.
   */
  public static LRUCache<? extends CachedItem> getCache(String tag) {

    // Walk the list of registred cache instance and look at the tag of those.
    List<CacheBase<? extends CachedItem>> cacheList = CacheBase.getCacheList();
    for (CacheBase<? extends CachedItem> cache : cacheList) {
      if (tag.equalsIgnoreCase(cache.getTag())) {
        return (LRUCache<? extends CachedItem>) cache;
      }
    }
    return null;
  }

  /**
   * Extract all possible parameter values by name from the request. If a parameter value is found,
   * set the according preference value on the given cache.
   * 
   * @throw Exception when values are found to be outside of sensible boundaries.
   */
  public static void setCacheParameters(HttpServletRequest request,
      LRUCache<? extends CachedItem> cache) throws Exception {

    String parm;
    int value;

    parm = request.getParameter(MAXAGE);
    if (parm != null && parm.length() > 0) {
      value = Integer.parseInt(parm);
      cache.setMaxAge(value);
    }

    parm = request.getParameter(MAXSIZE);
    if (parm != null && parm.length() > 0) {
      value = Integer.parseInt(parm);
      cache.setMaxSize(value);
    }

    parm = request.getParameter(MAXCLEANUP);
    if (parm != null && parm.length() > 0) {
      value = Integer.parseInt(parm);
      cache.setMaxRemovePerCleanup(value);
    }

    parm = request.getParameter(NEWCLEANUP);
    if (parm != null && parm.length() > 0) {
      value = Integer.parseInt(parm);
      cache.setRemovePerCleanup(value);
    }

    parm = request.getParameter(MAXDURATION);
    if (parm != null && parm.length() > 0) {
      value = Integer.parseInt(parm);
      cache.setMaxCleanupDuration(value);
    }
  }


  /**
   * Loop across all items in the cache looking for keys that match the given partial key. Matching is
   * done using String.contains(). A maximum of MAXFILTEREDKEYS matching keys are placed in the
   * list. The keys in the list are sorted before returning the list.
   */

  public static List<String> filterKeys(LRUCache<? extends CachedItem> cache, String keypart)
      throws ServletException {

    List<String> filtered = new ArrayList<String>();

    // Sanity check
    if (keypart == null || keypart.length() < 1) {
      filtered.add("No partial key for filtering");
      return filtered;
    }
    
    // Special case: * matches all keys
    boolean allKeys = "*".equalsIgnoreCase(keypart);

    Map<String, ? extends CachedItem> map = cache.getData();

    for (String key : map.keySet()) {

      if( allKeys || key.contains(keypart)) {
        filtered.add(key);
      }
      // We want to keep our list within limits
      if (filtered.size() > MAXFILTEREDKEYS) {
        break;
      }
    }
    Collections.sort(filtered);
    return filtered;
  }
  
  public static void showDoc( HttpServletRequest request, HttpServletResponse response) throws ServletException {
    
    String what = request.getParameter( WHAT);
    String fileName = "/faq-en.md";
    if( NUMBERS.equalsIgnoreCase(what)) {
      fileName = "/webinterface-en.md";
    }
    
    try {
      URL url = CacheAdminUtil.class.getResource( fileName);
      String markDownInput = IOUtils.toString( url, "UTF-8");
      Parser markdownParser = Parser.builder().build();
      Node document = markdownParser.parse( markDownInput);
      HtmlRenderer renderer = HtmlRenderer.builder().build();
      String htmlOutput = renderer.render(document);
      
      response.setContentType( "text/html");
      Writer writer = response.getWriter();
      writer.write( "<html><head></head><body>");
      writer.write( htmlOutput);
      writer.write( "</body></html>");
      writer.flush();

    } catch (IOException e) {
      throw new ServletException( e.getMessage());
    }
  }
}
