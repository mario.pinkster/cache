package nl.dimario.cache;

/**
 * This servlet presents usage statistics about all caches, can present one cache in detail
 * for interactively modifying its parameters on the fly, deleting objects from the cache,
 * and can present markdown documentation as HTML.
 */

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(urlPatterns = "/cache", name = "CacheAdminServlet", displayName = "Cache Monitor and Control")
public class CacheAdminServlet extends HttpServlet implements CacheAdminConst {

  private static final long serialVersionUID = 77337L;
  
  private static Logger LOG = LoggerFactory.getLogger(CacheAdminServlet.class);

  public CacheAdminServlet() {
    super();
  }

  /**
   * The GET request shows the overview page or a markdown document.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    
    // If a tag is present we transfer control to doPost() to display the cache detail page.
    String tag = request.getParameter( TAG);
    if( tag != null) {
      doPost(request, response);
      return;
    }
    
    // If a showdoc command is present we must show help or faq
    Command action = CacheAdminUtil.getAction(request);
    if( action.equals( Command.SHOWDOC)) {
      CacheAdminUtil.showDoc( request, response);
      return;
    }

    // Get the list of all caches that have been registered
    List< CacheBase<? extends CachedItem>> cacheList = CacheBase.getCacheList();

    // Place this list as anattribute in the  request so JSP can use it.
    request.setAttribute( CACHELIST, cacheList);
    RequestDispatcher rd = getServletContext().getRequestDispatcher("/cacheoverview.jsp");
    rd.forward(request, response);
  }

  /**
   * The POST request shows the detail page for one cache (the one named in
   * the "tag" parameter). If no command is given then no further action is
   * initiated and the result is a simple display of the detail page for
   * one cache instance. 
   * 
   * But if a command ("action") was given in POST request we will try and execute it.
   * 
   * If the request has no tag parameter or the value doesn't identify a cache,
   * we will go back to displaying the overview of caches.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // We need the tag of the cache. If not present at all we go back to the overview.
    String tag = request.getParameter( TAG);
    if( tag == null) {
      String message = "No \"tag\" parameter found in POST request, transferring to overview";
      LOG.info( message);
      request.setAttribute(MESSAGE, message);
      doGet(request, response);
      return;
    }
    
    // This is the cache for which details are shown and parameters will possibly be altered:
    LRUCache<? extends CachedItem> cache = CacheAdminUtil.getCache(tag);
    if( cache == null) {
      String message = "Tag \"" + tag +"\" is not recognized as the name of a cache instance, transferring to overview";
      LOG.info( message);
      request.setAttribute(MESSAGE, message);
      doGet(request, response);
      return;
    } 
    
    // See what command we must execute, if any
    Command command = CacheAdminUtil.getAction( request);
    
    try {
      switch( command) {
        
        default: {
          // Default we only display the detail page without doing anything else.
        } break;
        
        case SETPARAMS: {
          CacheAdminUtil.setCacheParameters( request, cache);
        } break;
        
        case RESETCOUNT: {
          cache.resetCount();
        } break;
        
        case CLEANUPNOW: {
          cache.cleanup();
        } break;
        
        case DELETEKEY: {
          String keytodelete = request.getParameter( KEYTODELETE);
          cache.removeOne(keytodelete);
        } // INTENTIONAL FALL THROUGH
        case SHOWKEYS: {
          
          // Read the partial key value from the request parameters 
          // and copy it back as an attribute for the JSP to use.
          String keypart = request.getParameter( KEYPART);
          if( keypart != null && keypart.length() > 0) {
            request.setAttribute( KEYPART, keypart);
          }
          
          // Show the result (possibly after deletion of one item) of the partial key filter
          List<String> filteredKeys = CacheAdminUtil.filterKeys(cache, keypart);
          request.setAttribute(KEYLIST, filteredKeys);
          
          // See if the list was truncated due to too many matching keys
          if( filteredKeys.size() > MAXFILTEREDKEYS) {
            String message = String.format( "There are more keys that match the regular expression (only %d shown)", MAXFILTEREDKEYS);
            request.setAttribute(MESSAGE, message);
          }
        } break;
        
        case CLEARALL: {
          cache.clearAll();
        } break;
      }
      
    } catch( Exception x) {
      String message = "ERROR: " + x.getMessage();
      LOG.error( message);
      request.setAttribute(MESSAGE, message);
    }
    
    request.setAttribute(CACHE, (LRUCache<? extends CachedItem>) cache);
    RequestDispatcher rd = request.getRequestDispatcher("/cachedetail.jsp");
    rd.forward(request, response);
  }
}
