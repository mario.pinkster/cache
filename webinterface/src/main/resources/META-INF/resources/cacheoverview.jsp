<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	pageContext.setAttribute("cachelist", request.getAttribute("cachelist"));
	pageContext.setAttribute("message", request.getAttribute("message"));

	// Set refresh, autoload time as 3 seconds
	response.setIntHeader("Refresh", 3);
%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Caches</title>

<style>
table, th, td {
    border: 1px solid gray;
    border-collapse: collapse;
}
</style>

</head>
<body">

<H1>Caches overview</H1>

<span style="color:red;">${message}</span>

<p>&nbsp;</p>
<p>&nbsp;</p>
<table style="width:60%">
<tr>
<th>Cache</th>
<th>Size</th>
<th>Total</th>
<th>Hits</th>
<th>Misses</th>
<th>Expired</th>
<th>Cleanups</th>
<th>Duration</th>
<th>Avg age</th>
<th>Max items</th>
<th>Max age</th>
</tr>
<c:forEach var="cache" items="${cachelist}">
<tr>
	<td><a href="cache?tag=${cache.tag}">${cache.tag}</a></td>
	<td>${cache.size}</td>
	<td>${cache.getCount}</td>
	<td>${cache.hitCount}</td>
	<td>${cache.missCount}</td>
	<td>${cache.expiredCount}</td>
	<td>${cache.cleanupCount}</td>
	<td>${cache.lastCleanupDuration}</td>
	<td>${cache.avgCleanupAge}</td>
	<td>${cache.maxSize}</td>
	<td><c:out value="${fn:substringBefore( cache.maxAge div 1000, '.')}" /></td>
</tr>
</c:forEach>
</table>
<br>
<a href="cache?action=showdoc&what=numbers">What do these numbers mean?</a>&nbsp;&nbsp;&nbsp;
<a href="cache?action=showdoc&what=faq">I have a question</a>
</body>
</html>
