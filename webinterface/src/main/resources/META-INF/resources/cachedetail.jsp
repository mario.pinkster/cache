<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%
	pageContext.setAttribute("message", request.getAttribute("message"));
	pageContext.setAttribute("cache", request.getAttribute("cache"));
	pageContext.setAttribute("keylist", request.getAttribute("keylist"));
	pageContext.setAttribute("keypart", request.getAttribute("keypart"));
	pageContext.setAttribute("keypartmore", request.getAttribute("keypartmore"));

	// Set refresh, autoload time as 3 seconds
	// response.setIntHeader("Refresh", 3);
%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Caches</title>

<style>
table, th, td {
    border: 1px solid gray;
    border-collapse: collapse;
}
</style>

</head>
<body onLoad="setRefresh()">

<H1>Details voor ${cache.tag}</H1>

<p>&nbsp;</p>
<span style="color:red;">${message}</span>
<p>&nbsp;</p>

<form action="cache" method="POST" name="cacheForm">
<input  type="hidden" name="tag" value="${cache.tag}" />
<input  type="hidden" id="action" name="action" value="display" />
<input  type="hidden" id="keytodelete" name="keytodelete" value="" />

<table border="0">
<tr>
	<td>Size</td>
	<td>Total</td>
	<td>Hits</td>
	<td>Misses</td>
	<td>Expired</td>
	<td>Cleanups</td>
	<td>Last Duration</td>
	<td>Avg cleanup age</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>${cache.size}</td>
	<td>${cache.getCount}</td>
	<td>${cache.hitCount}</td>
	<td>${cache.missCount}</td>
	<td>${cache.expiredCount}</td>
	<td>${cache.cleanupCount}</td>
	<td>${cache.lastCleanupDuration}</td>
	<td>${cache.avgCleanupAge}</td>
	<td><button type="submit" name="btnRefresh" onClick="refresh()">Refresh</button>
</tr>
</table>
<br>
<a href="cache?action=showdoc&what=numbers">What do these numbers mean?</a>&nbsp;&nbsp;&nbsp;
<br>
<br>


<table border="1">
<tr>
<td>Parameter</td>
<td>Current value</td>
<td>New value</td>
<td>&nbsp</td>
</tr>

<tr>
	<td>Max items in cache</td>
	<td>${cache.maxSize}</td>
	<td><input type="text" name="maxsize" /></td>
	<td><button type="submit" name="setMaxSize" onClick="setParams();" />Set</td>
</tr>
	
<tr>
	<td>Max age per item in seconds</td>
	<td><c:out value="${fn:substringBefore( cache.maxAge div 1000, '.')}" /></td>
	<td><input type="text" name="maxage" /></td>
	<td><button type="submit" name="setMaxAge" onClick="setParams();" />Set</td>
</tr>

<tr>
	<td>Max number to clean up in any run</td>	
	<td>${cache.maxRemovePerCleanup}</td>
	<td><input type="text" name="maxcleanup" /></td>
	<td><button type="submit" name="setMaxClenaup" onClick="setParams();" />Set</td>
</tr>

<tr>
	<td>New number of items to cleanup in next run</td>	
	<td>${cache.removePerCleanup}</td>
	<td><input type="text" name="newcleanup" /></td>
	<td><button type="submit" name="setNewCleanup" onClick="setParams();" />Set</td>
</tr>

<tr>
	<td>Max duration for one cleanup run in <b>micro</b>seconds</td>	
	<td>${cache.maxCleanupDuration}</td>
	<td><input type="text" name="maxduration" /></td>
	<td><button type="submit" name="setMaxDuration" onClick="setParams();" />Set</td>
</tr>

</table>
<br>
<br>
<br>

<% 
	List< String> keylist = (List<String>) request.getAttribute( "keylist");
%>
	<table border="1">
	<tr>
		<td colspan="8">
			Display keys containing (use <big>*</big> for all keys) :
			<input type="text" size="10" name="keypart" value="${keypart}"/> 
			&nbsp;&nbsp;&nbsp;&nbsp; 
			<button type="submit" onClick="showKeys()">Show keys!</button>
			
		</td>
	</tr>
<%
	if( keylist != null) {
%>
	<tr>
		<td colspan="8">&nbsp;</td>
	</tr>
	<%
		for( int i=0; i<keylist.size(); i+=8) {
	%>
	<tr>
		<%
			int start = i;
			int end = i+8;
			if( end >= keylist.size()) {
			  end = keylist.size();
			}
			for( int ix=start; ix<end; ix++) {
			  String item = (ix < keylist.size()) ? keylist.get( ix) : "&nbsp;";
		%>
			<td>
			<%=item %>&nbsp;&nbsp;<a href="javascript:deleteKey('<%=item%>')">d</a>
			</td>
		<% 
			}
		%>
	</tr>
<%
		}
	}
%>
</table>

<br>
<br>
<br>


<table>
<tr>
	<td><button type="button" name="btnBack" onClick="location.href='cache'" >Back to overview</button>
	<td><button type="submit" name="btnCleanupNow" onClick="cleanupNow()">Cleanup now</button>
	<td><button type="submit" name="btnResetCount" onClick="resetCount()">Reset counters to zero</button>
	<td><button type="submit" name="btnClearAll" onClick="clearAll()">Remove all items from the cache</button>
</tr>
</table>




</form>

<script type="text/javascript">

function setRefresh() {
//	setTimeout( 'refresh()', 5000);
}

function refresh() {
	var action = document.getElementById( "action");
	action.value = "display";
	document.cacheForm.submit();
}

function setParams() {
	var action = document.getElementById( "action");
	action.value = "setparams";
	document.cacheForm.submit();
}

function cleanupNow() {
	var action = document.getElementById( "action");
	action.value = "cleanupnow";
	document.cacheForm.submit();
}

function clearAll() {
	var action = document.getElementById( "action");
	action.value = "clearall";
	document.cacheForm.submit();
}

function resetCount() {
	var action = document.getElementById( "action");
	action.value = "resetcount";
	document.cacheForm.submit();
}

function showKeys() {
	var action = document.getElementById( "action");
	action.value = "showkeys";
	document.cacheForm.submit();
}

function deleteKey( key) {
	var action = document.getElementById( "action");
	action.value = "deletekey";
	var keytodelete = document.getElementById( "keytodelete");
	keytodelete.value = key;
	document.cacheForm.submit();
}

</script>
</body>
</html>
