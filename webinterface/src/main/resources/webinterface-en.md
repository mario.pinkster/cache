## What does the information in the web interface mean?

***Tag* or name of the cache.**  
Your application may create multiple instances of the LRUCache object in order to cache different types of objects you need to cache (with different caching parameters such as maximum number of items and maximum age of items). In the web interface, and in the logging, a single instance of LRUCache is identified by the tag that you set when creating the instance. One instance of the cache stores only a single type of cached objects. The tag value must be unique and this is enforced when setting it.

**Size**  
This is the current number of objects in the cache. The size grows by virtue of the application putting new items in the cache, and it shrinks by virtue of the cache itself performing cleanup runs. You can also forcibly remove items from the cache with the web interface.

In Java, it is difficult to express the size of objects in bytes and hence it is also difficult to express the maximum size of a cache in terms of bytes used in memory. Instead, we use the number of items in a cache as a measure of how much memory it uses. There is no easy way to determine an exact relation between the number of objects and the memory the cache uses, because there is no exact way of knowing how much memory a single objects takes up in memory.

**Max Size  (Max Items)**  
This is the maximum number of items that can be stored in this instance of the cache. The general idea is that you can roughly control the memory usage of your program by setting a value for this number.

When the actual number of items that the application attempts to add to this cache exceeds its maximum size, a cleanup run is performed. The purpose of the cleanup run is to remove stale and relatively unused items from the cache in order to create space for new items.

There is no hard guarantee that the actual number of items in the cache will remain at or below the maximum. When the cache is near maximum capacity and several threads attempt to add new items at neary the same time it is possible that individually they will find enough space left while collectively they exceed the maximum number of items. Such a situation should be corrected by the next cleanup run that comes along.

The Max Size parameter can be changed by means of the web interface. Be careful when reducing this size severely to a value far below the current size. In theory, cleanup runs will start working hard to shrink the number of items in the cache. In practice this is the kind of situation where design flaws in the cleanup algorithm are most likely to manifest themselves with drastic consequences.

**Total**  
This is the total number of get() calls for the cache (regardless of the result – found or not, and if found, expired or not).


**Hits**  
This is the number of get() calls for the cache that were successful. A successful call finds the item in the cache while the item is not expired. The purpose of a cache is to have a high ratio of hits versus total calls.

**Misses**  
This is the number of get() calls for the cache that did not find the item for the requested key in the cache.

**Expired**  
This is the number of get() calls for the cache that did find the item in the cache but then determined that it had expired. From the point of view of the application this is the same as a miss, but for cache tuning purposes it is notable information to have. When a cache has a lot of expireds this means that either your cache size is too large (allowing many expired items to live in the cache while they have no good reason to) or you allow too low a Max Age for your items in that cache. Or both, of course. Either way this would be reason for further analysis.

**Cleanups**  
This is the total number of cleanup runs that have been run for this cache. Whether a run was initiated by hand from the web interface or automatically because the cache was found full during a put() is not counted separately; both contribute to this number.

When this number rapidly increases this is a sign that the cache is too small with respect to the actual number of items that the application needs to use during normal operation.
On the other hand when this number increases slowly or not at all, this means that all items of this particular type that your application needs during its normal operation are eventually stored in the cache. Occasionally a get() will find an item to be expired and refresh it on the spot, without much need for cleanup runs. Whether this is desirable depends of course on the type of object. For instance, caching selection lists that never change in this way is a good idea, but caching order details for varying customers in this way probably is not.

**(Last) Duration, Max Duration**  
Duration is the actual duration of the last cleanup run ***in microseconds***. Max Duration is the target value for the maximum duration of any cleanup run. A target value that is not always honored, or so it turns out. The autotuning algorithm of the cache tries to tune other parameters in such a way that this target value is respected, but does not always succeed in doing so.

The Last Duration value is an actual measurement taken from the last time a cleanup run was performed. The relevance of this number is that during this amount of time, other threads than the one the cleanup runs in would be able to call put() on this cache but the offered items were ignored. This is of course not what we would want to happen extensively.

So the point is to keep the actual time a cleanup run takes as short as possible in order to not interfere with items being put() into the cache, but also long enough for the cleanup run to be able to perform an acceptable amount of cleanup.

You can adjust the value of Max Duration using the web interface. Please note that this value must be entered  ***in microsecond units***.

In the wild it turns out that the actual time a cleanup run takes fluctuates rather strongly. The idea that it takes more time to create a larger list of items that must be deleted turns out to be more of a plausible assumption that a given law of nature.

**Max Age**  
This is the maximum time (in seconds) that an item is considered to be viable after it has been put() in the cache. For instance, when you make a database query, this is expensive in terms of processing time so it would be nice if you could cache the result of your query and avoid spending all this processing each time you need to access this result. On the other hand, other processes outside of your application (or perhaps other users of your application) may update the contents of the database at unpredictable times making the result you had cached no longer the most recent information available. You (the application programmer) will have to make educated guesses and compromize between reusing database results for speed and risk using stale information. A way to express the compromize is to affirm a maximum lifetime for the results in your cache. After this time this particular piece of information in the cache is considered no longer valid and will not be used by your application anymore.

The value of Max Age is used by the get() operation to determine if the information it found for a certain key in the cache structure is still viable. If not, the get() operation will remove the item and report that it could not find it. The same value for Max Age will also be used by the cleanup run to determine whether an item is expired and can be deleted in order to make room for new items that must be put().

The cleanup is performed in the same thread that wants to do the get() and subsequent put().

**Average Age**  
This is the measured average age of items that were deleted from the cache during the last cleanup run. This measurement must be considered in relation to Max Age for the cache, which is a value set by the application programmer. When the Average Age is much less than the Max Age, this means that items are deleted from the cache long before they expire. They are deleted from the cache because the room is needed for new items, and they have not expired because they haven’t had the time to reach the intended Maximum Age.

Being deleted to make new space is a sign that the application uses a lot more items of this particular data type than the cache allows for (i.e. the cache is too small).
Not being expired is a sign that either the items are allowed to live much too long or again, new arrivals push them out of the cache long before their time is up (the cache is too small).

The objective of this measurement is to offer insight in what is happening in a cache instance. When you note a large discrepancy between the Average Age and the Max Age this is something that needs investigation.

**Number of items to clean up in  the next run**  
This is the target value for the number of items that the scanning phase of the next cleanup run will try to place on the “death list”. Items are selected for this list on grounds of being expired, or if not enough expired items are present in the cache, on grounds of being least used (having been retrieved from the cache longest ago).

The value of this number is not fixed. It is autotuned at the end of each cleanup run in order to make the next cleanup run do as efficient a job as possible by compromizing between the amount of items to delete and the amount of time it takes to find them. But please note that the expectation that more cleanup takes longer to perform is just that: an expectation. Measurements indicate that this expectation does not always hold true. All in all, the cleanup run is a rather unpredictable process.

The autotuning of this cleanup parameter is subject to boundaries. It can not be set lower than three, and not higher than the “Max Value” parameter (see next item in this explanation).

You can set a value for this parameter via the web interface and thus influence the amount of work the next cleanup run will perform. Be advised that your input is validated against the mentioned boundaries, and also that any value you enter here will possibly be changed at the end of the next cleanup run. You can monitor the change in value via the web interface.

**Max Number of items to clean up in a cleanup run**  
This is the upper boundary for the number of items to place on the “death list” in the scanning phase of the cleanup run. Neither autotuning nor entry via the web interface are allowed to set the number to scan for higher than this maximum.

This upper boundary is not changed by the autotuning of the cleanup run, it can only be set at cache initialization or via the web interface. In either case the highest value you can set for this upper boundary is fifty percent of the Max Size of the cache. The lowest value that can be set for this upper boundary is three. The default value set at intialization of the cache is ten percent of the maximum size of the cache.

When you change the Max Size of the cache via the web interface a new value for this upper boundary is set automatically to ten percent of the new Max Size.