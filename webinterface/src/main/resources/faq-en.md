**How much memory does an item in the cache use?**  
This is not easy to determine. The problem is that Java has no simple system call that returns the size of an object or data structure.

The smallest thing that you can cache is a plain CachedItem object, which consists of a Java Object plus two long values (one long is 8 bytes). Depending on the implementation of the JVM,  a bare Object is anywhere from 16 to 32 bytes.

The size of a realistic payload CachedItem varies with what you choose to add to your object types in terms of attributes and sub structures. Measurements are possible, for instance by using a tool such as VisualVM but to be honest I haven’t had the necessity yet to perform in depth analysis.

For those who want to investigate, [here is a link I found some time ago](https://stackoverflow.com/questions/258120/what-is-the-memory-consumption-of-an-object-in-java)

**Can I store different types of Objects in the same cache instance?**  
No. At least, that is not how I intended it to be used. An application will of course have different types of Objects that must be cached but these will have different caching parameters. For instance, the result of an SQL  lookup for available stock names may be valid for the rest of the day, whereas the price quote for a single stock may be invalidated as soon as the HTTP request ends.

The way to deal with this is to create different instances of the cache for different types of Objects. So at startup, your application must create and configure as many instances of the LRUCache as there are Object types to be cached, and throughout its operation take care to store Objects in the correct instance of each cache.

This is partially ensured by making LRUCache a generic thing, which means that when you want to create an instance of it you need to provide a Type for the items to be stored in this particular instance. In this way your compiler will throw errors when you inadvertently try to store a String in a LRUCache instance that wants to store Date objects. It will signal a syntax error on the line where you put() the erroneous Type in the cache.

Furthermore each instance of the LRUCache has a tag (or name) that shows up in all logging messages for that instance. This tag is also the identification for a cache instance in the web interface.

**How is this cache different from other caching systems or libraries?**  

* It is not very mature yet so chances are you will discover bugs.
* It only caches to memory. There is no backing on the file system or other ways to persist cached items through a restart of the application.
* Object types that you want to cache must extend CachedItem. This adds the cost of two longs per cached payload object. Other caching systems wrap the cached objects in a container. This adds the cost of one containter object per cached payload object.
* It has a web interface that you can use to monitor cache behaviour at runtime in production.
* You can also use the web interface to ***alter*** cache parameters at runtime in production. You can grow or shrink the maximum number of items and also change the maximum time to live for items in the cache.
* You can also use the web interface to remove single items from the cache, or totally empty one cache instance.
* Cleanup happens in the context of a *put()* call. There is no separate daemon thread running in the background that periodically scans all cache instances to remove stale items.
* The cleanup process deletes multiple stale objects from the cache per cleanup run instead of a single one. The actual number of cleaned up stale objects is variable and autotuned by the cleanup process.

**What happens when I add a new item to a cache that is filled to maximum capacity?**  
In general, trying to read something from the cache that is not yet in it will cause your application to create whatever it is you’re looking for and then put() it in the cache for future get()s to find it. The call to put() first checks if there is room left in the cache and triggers a cleanup run if not. The cleanup run will scan some or all items in the cache and build a list of items that may be deleted from the cache to create space and then delete those items. Because all of this happens in the context of the thread that was innocently trying to get() something from the cache, that thread will experience a slight delay. Mechanisms and algorithms are in place to ensure that this delay has an (user defined) upper boundary. For all practical purposes it is in the order of nanoseconds and of course it does not happen every time you try to read something from the cache, unless your configuration is ill matched to the actual number of items that need to be cached and/or their maximum time to live. A web interface exists to let you monitor and tweak what happens with a cache at runtime in production situations.

**How does the cleanup run determine which items may be deleted?**  
Each cleanup run looks for a certain number of items (at least three) that can be removed. An item that has outlived its maximum time to live is an obvious candidate. When the cleanup run finds enough of those, it terminates scanning and goes on to delete them. But it is also possible that there are not enough items past their time to live in the cache to satisfy the number this cleanup run is looking for. In that case the list is augmented with items that have been gotten from the cache in the most distant past. These items are “least recently used” and this is where the abbreviation LRU comes from.

**How many items does the cleanup run clean up?**  
This number is not fixed. At the end of each cleanup run, some calculations are performed to tune this number so that a next cleanup run will try and remove an ideal number of expired or old items. The ideal number tries on the one hand to clean up as many stale items as possible and on the other hand not take too long as a cleanup run prolongs the current get() operation that triggered it.

Currently the default number of items to cleanup per run is set at cache initialization to approximately three percent of the maximum number of items in the cache. After each cleanup run the algorithm looks at how long it took to perform the cleanup and how many items were deleted from the cache and adjust the number for the next cleanup run accordingly. If the cleanup was performed completely within the user defined maximum time, we can try and delete more items next time (on the assumption that it takes longer to find more items to delete). If, on the other hand, we had to cut a cleanup run short because it exceeded the maximum allowed time, this means we tried to do too much and will look for less items to clean up the next time around.

After calculating a new value it is then checked against a sensible minimum and maximum value to prevent the cleanup from running wild and emptying the whole cache or underperforming to the point that no items whatsoever are deleted from the cache during a cleanup.

Currently the sensible maximum for the number of items to delete during one cleanup is about ten percent of the maximum cache size, and the sensible minimum is three items regardless of the maximum cache size.

The webinterface allows you to change both the current number of items to cleanup in one run and also the maximum number that the autotuning algorithm is allowed to set. The minimum number remains at three and cannot be changed. In addition, you can adjust how much time one cleanup run is allowed before being forcibly pushed from the scanning stage to the deleting stage.

**What happens with an item that has exceeded its time to live?**  
When a stale item is encountered by the scanning stage of a cleanup run, it is added to the list of items that will be deleted (and will be deleted in the deleting stage of that cleanup run).
When a stale item is encountered by the normal get() operation of the cache, the get() operation will delete this item from the cache and return null in order to signal that no item was found for the given input key parameter.

The stale item remains in the cache until either of these events occur. But it is also possible to forcibly remove one item or all items from the cache using the web interface. This operation actually does remove items from the cache (as opposed to merely marking items for deletion).

**Is it thread safe? What happens if another thread accesses the cache during a cleanup run?**  
At the start of a cleanup run, a flag is set on the cache that inhibits put() operations. When another thread performs a put() while this flag is up, the put() will be ignored silently. That is, it will not change the contents of the cache (the item to put is ignored) and it will return normally as if the put had succeeded. This is not a problem, because the next time you want the item for this key the get() won’t find it and you will be right where you started before the previous put() was ignored silently. It is not an error, but of course it will affect performance when it happens too often.

A *get()* call to the cache will not be affected by the “cleanup is running” flag. When deleting items from the cache, the cleanup run uses normal Java locking mechanisms for removing objects from the cache structure. This means that the get() is blocked by the standard JVM thread synchronization mechanisms each time the cache is locked in order to remove a single item. If the get() reaches the cache structure before the item is removed, the item is found and the get() succeeds (unless the item is expired). If the get happens to reach the cache structure after removal of the item, it’s not there and the get() fails.

So get() operations from other threads are not blocked by the cleanup (except for normal JVM synchronization during actual deletion of an item) and neither are put() operations, although these have no effect at all while a cleanup operation is running.

**What happens when I lower the Maximum Age for items in a cache on runtime?**  
Items in the cache record the exact time when they were placed in the cache. This timestamp is then used in combination with the Maximum Age for items in the cache and the current time to determine wether they are stale when a get() operation retrieves them, and to determine staleness during a cleanup run.

When you change the Maximum Age for items in a cache, the individual timestamps of “birth” of the items that are already in the cache is not altered in any way. But the calculation to determine if an item is stale will use the new Maximum Age for any and all items it is applied to.

So a change in Maximum Age immediately affects all items in the cache, while the items themselves are not altered in any way.
