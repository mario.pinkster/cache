package nl.dimario.razz;

import org.junit.Test;

import nl.dimario.razz.RandomData;

public class RandomDataTest {

	@Test
	public void tetsuo() {
		
		String s;
		
		s = RandomData.make( "upperletter");
		report(s);
		
		s = RandomData.make( "lowerletter");
		report(s);
		
		s = RandomData.make( "uppermixed");
		report(s);

		s = RandomData.make( "lowermixed");
		report(s);

		s = RandomData.make( "numberonly");
		report(s);
	}

	private void report( String s) {
		System.out.println("\n\n\n");
		System.out.println( s.length());
		System.out.println("\n\n");
		System.out.println( s);
		System.out.println("\n\n\n");
	}
}
