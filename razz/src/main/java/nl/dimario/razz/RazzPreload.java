package nl.dimario.razz;

/**
 * This class is currently not used in the razz application.
 * Its intention is to preload the cache instances with some data
 * at server startup.
 */
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RazzPreload {
	
    protected final static Logger LOG = LoggerFactory.getLogger(RazzPreload.class);
    
    private RazzSource source;

    public void init() {

        LOG.info("RazzPreload is being prepared");
        List<String> preloadList = makePreloadList( 13);
        RazzPreloadRunner razzPreloadRunner = new RazzPreloadRunner( source, LOG, preloadList);
        Thread preloadThread = new Thread( razzPreloadRunner);
        preloadThread.setName("RazzPreloadRunner");
        preloadThread.start();
    }
    
    private List<String> makePreloadList( int count) {
    	
    	List<String> result = new ArrayList<String>();
    	String key = null;
    	for( int i=0; i<count; i++) {
    		key = String.format( "%03X", i);
    		result.add(key);
    	}
    	return result;
    }
    
	public RazzSource getSource() {
		return source;
	}

	public void setSource(RazzSource source) {
		this.source = source;
	}
}

class RazzPreloadRunner implements Runnable {
	
	private Logger LOG;
    private RazzSource source;

	protected List<String> preloadKeys;
	
	protected RazzPreloadRunner( RazzSource source, Logger LOG, List<String> preloadKeys) {
		this.source = source;
		this.LOG = LOG;
		this.preloadKeys = preloadKeys;
	}
	
	public void run() {
		try {
			LOG.info( "Preloading data starts");
			Thread.sleep( 2000);
			for( String key: preloadKeys) {
				for( RazzSource.DATATYPE datatype: RazzSource.DATATYPE.values()) {

					LOG.debug( String.format( "Preloading data for key=\"%s\", datatype=\"%s\"", key, datatype));
					source.get( datatype.toString(), key);
				}
				Thread.sleep( 1500);
			}
			LOG.info( "Preloading data ready");
		} catch( InterruptedException ix) {
			LOG.warn( "Interrupted!!");
		}
	}
}
