package nl.dimario.razz;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import nl.dimario.cache.CachedItem;

public class CachedThingy extends CachedItem  {

	private List<String> logList;
	private String data;
	
	public CachedThingy( String data) {
		logList = new ArrayList<String>();
		this.data = data;
		this.log( "Thingy: Created new");
	}

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public void log( String msg) {
		logList.add( String.format( "%s - %s", sdf.format(new Date()), msg));
	}
	
	public String report() {
		
		StringBuilder sb = new StringBuilder();
		for( int i = logList.size()-1; i > -1; i--) {
			sb.append( logList.get(i));
			sb.append( "<br/>\n");
		}
		return sb.toString();
	}
	
	public String getData() {
		log( "Thingy: Data retrieved");
		return data;
	}

	public void setData(String data) {
		log( "Thingy: Data updated");
		this.data = data;
	}
}
