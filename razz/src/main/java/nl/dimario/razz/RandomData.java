package nl.dimario.razz;

/**
 * Some static methods to create nonsense Strings which have certain characteritics. 
 */

import java.util.Random;
import org.apache.commons.lang3.RandomStringUtils;

public class RandomData {

  private static Random random;

  public static String make(String datatype) {

    if (RazzSource.DATATYPE.upperletter.toString().equalsIgnoreCase(datatype)) {
      return makeRandomText(true, true, false);
    }
    if (RazzSource.DATATYPE.uppermixed.toString().equalsIgnoreCase(datatype)) {
      return makeRandomText(true, true, true);
    }
    if (RazzSource.DATATYPE.lowerletter.toString().equalsIgnoreCase(datatype)) {
      return makeRandomText(false, true, false);
    }
    if (RazzSource.DATATYPE.lowermixed.toString().equalsIgnoreCase(datatype)) {
      return makeRandomText(false, true, true);
    }
    if (RazzSource.DATATYPE.numberonly.toString().equalsIgnoreCase(datatype)) {
      return makeRandomText(false, false, true);
    }
    throw new RuntimeException("Onbekend datatype: " + datatype);
  }

  public static int randomGaussian(double median, double variance) {

    if (RandomData.random == null) {
      Random rd = new Random();
      synchronized (RandomData.class) {

        RandomData.random = rd;
      }
    }
    double val = -1.0;
    while (Double.compare(val, 1.0) < 1) {
      double delta = random.nextGaussian() * variance;
      val = median + delta;
    }
    return (int) Math.round(val);
  }

  private static String makeRandomText(boolean upperCase, boolean useLetters, boolean useNumbers) {

    int words = randomGaussian(514.8, 189.2);
    int wordsPerLine = randomGaussian(7.8, 2.8);
    StringBuilder sb = new StringBuilder(8192);

    String sep = "";
    for (int i = 0; i < words; i++) {

      sb.append(sep);
      addRandomWord(sb, upperCase, useLetters, useNumbers);
      if (wordsPerLine < 1) {
        wordsPerLine = randomGaussian(7.8, 2.8);
        sep = "";
        sb.append('\n');
      } else {
        sep = " ";
        wordsPerLine--;
      }
    }

    return sb.toString();
  }


  private static void addRandomWord(StringBuilder sb, boolean upperCase, boolean useLetters,
      boolean useNumbers) {

    int len = randomGaussian(4.75, 3.1);

    String random = RandomStringUtils.random(len, useLetters, useNumbers);

    if (upperCase) {
      random = random.toUpperCase();
    } else {
      random = random.toLowerCase();
    }

    sb.append(random);
  }
}
