package nl.dimario.razz;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.dimario.cache.CacheBase;
import nl.dimario.cache.LRUCache;

public class RazzSource {

  private static Logger LOG = LoggerFactory.getLogger(RazzSource.class);

  public enum DATATYPE {
    upperletter, lowerletter, uppermixed, lowermixed, numberonly;
  };


  @SuppressWarnings({"unchecked", "unused"})
  public void initConfiguration() throws Exception {

      LOG.info("The memory is being initialized...");

      LRUCache<CachedThingy> cache;
      String tag;

      tag = DATATYPE.upperletter.toString();
      cache = (LRUCache<CachedThingy>) LRUCache.create(tag, 100, 60);

      tag = DATATYPE.lowerletter.toString();
      cache = (LRUCache<CachedThingy>) LRUCache.create(tag, 500, 180);


      tag = DATATYPE.uppermixed.toString();
      cache = (LRUCache<CachedThingy>) LRUCache.create(tag, 50, 15);

      tag = DATATYPE.lowermixed.toString();
      cache = (LRUCache<CachedThingy>) LRUCache.create(tag, 1000, 600);


      tag = DATATYPE.numberonly.toString();
      cache = (LRUCache<CachedThingy>) LRUCache.create(tag, 20000, 3600);
  }

  @SuppressWarnings("unchecked")
  public CachedThingy get(String datatype, String key) {

    LRUCache<CachedThingy> cache = (LRUCache<CachedThingy>) CacheBase.getCache( datatype);
    if (cache == null) {
      throw new RuntimeException("Unknown datatype: " + datatype);
    }
    CachedThingy thingy = cache.get(key);

    if (thingy == null) {
      String data = RandomData.make(datatype);
      thingy = new CachedThingy(data);
      thingy.log("Source: Created new %s thingy after cache miss, key=" + key);
      cache.put(key, thingy);
    } else {
      thingy.log("Source: cache hit for key=" + key);
    }
    return thingy;
  }

  public CachedThingy getNew(String datatype) {
    String data = RandomData.make(datatype);
    CachedThingy thing = new CachedThingy(data);
    thing.log("Source: Created new thingy on demand");
    return thing;
  }

  @SuppressWarnings("unchecked")
  public void put(String datatype, String key, CachedThingy thingy) {
    
    thingy.log("Source: store thingy for key=" + key);
    LRUCache<CachedThingy> cache = (LRUCache<CachedThingy>) CacheBase.getCache( datatype);
    if (cache == null) {
        throw new RuntimeException("Unknown datatype: " + datatype);
    }
    cache.put(key, thingy);
  }
}

