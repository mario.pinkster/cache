package nl.dimario.razz;

/**
 * This is the server part of the razz webapplication. It creates 
 * CachedThingy objects or gets them from the cache. It receives
 * requests from the browser part of the application and tries to 
 * serve them. A request contains a command, a datatype, and if applicable
 * for the command also a key and data which has been edited in the browser.
 * 
 * The servlet uses a RazzSource to interface with the cache and storage of
 * the nonsensical text strings.
 */
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(urlPatterns = "/razz", name = "razzServlet", displayName = "Razzmanian Devil")
public class RazzServlet extends HttpServlet {

  private static final Logger LOG = LoggerFactory.getLogger(RazzServlet.class);

  private static final long serialVersionUID = 1L;

  private static final String KEY = "key";
  private static final String DATA = "data";
  private static final String LOGGIE = "loggie";
  private static final String DATATYPE = "datatype";

  private static final String COMMAND = "command";
  private static final String LOAD = "load";
  private static final String NEW = "new";
  private static final String SAVE = "save";

  private static RazzSource sourceInstance;
  
  @Override
  public void init() throws ServletException {
    getSource();
  }
  
  
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    request.setCharacterEncoding("UTF-8");
    request.setAttribute(LOGGIE, "");

    String command = request.getParameter(COMMAND);
    if (command == null || command.length() < 1) {
      LOG.debug("No \"command\" parameter found, assuming default \"load\"");
      command = LOAD;
    } else {
      LOG.debug("\"{}\" parameter found, the value is \"{}\" ", COMMAND, command);
    }

    // See what data we must cough up (the KEY identifies a particular nonsense text).
    String key = request.getParameter(KEY);
    if (key != null) {

      key = key.trim();

      LOG.debug("\"{}\" parameter found, the value is \"{}\"", KEY, key);
    }

    // See which data type we are looking for. This determines the cache we will be using.
    String datatype = request.getParameter(DATATYPE);
    if (datatype != null) {

      datatype = datatype.trim();

      LOG.debug("\"{}\" parameter found, the value is \"{}\"", DATATYPE, datatype);
    } else {
      datatype = RazzSource.DATATYPE.upperletter.toString();
      LOG.debug("No \"{}\" parameter found, assuming default \"{}\"", DATATYPE,
          datatype);
    }

    String data = "** Not initialized **";
    String loggie = data;

    if (LOAD.equalsIgnoreCase(command) || SAVE.equalsIgnoreCase(command)) {
      if (key != null && key.length() > 0) {
        CachedThingy thingy = getSource().get(datatype, key);
        if (thingy == null) {
          throw new ServletException("Data thingy is null! This is impossible!");
        }
        data = thingy.getData();
        loggie = thingy.report();
      }
    }

    request.setAttribute(DATA, data);
    request.setAttribute(KEY, key);
    request.setAttribute(LOGGIE, loggie);
    request.setAttribute(DATATYPE, datatype);
    RequestDispatcher rd = request.getRequestDispatcher("razz.jsp");
    rd.forward(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    request.setCharacterEncoding("UTF-8");
    request.setAttribute(LOGGIE, "");

    // First check for a command. This is mandatory for the POST request.
    String command = request.getParameter(COMMAND);
    if (command == null || command.length() < 1) {
      throw new ServletException(
          "No \"COMMAND\" parameter found in POST, this servlet is unable to handle the request.");
    }
    LOG.debug("\"{}\" parameter found, the value is \"{}\" ", COMMAND, command);

    // See which data TYPE we must spoon up (case sensitive and mandatory!)
    String datatype = request.getParameter(DATATYPE);
    if (datatype != null) {

      datatype = datatype.trim();
      LOG.debug("\"{}\" parameter found, the value is \"{}\"", DATATYPE, datatype);

    } else {
      throw new ServletException(
          "No \"DATATYPE\" parameter found in POST, this servlet is unable to handle the request.\"");
    }

    // If it is a LOAD request we will handle it by acting as it it was a GET instead of a POST
    if (LOAD.equalsIgnoreCase(command)) {
      doGet(request, response);
      return;
    }
    // If the command is NEW we musct create and cahce a new data thingy.
    if (NEW.equalsIgnoreCase(command)) {
      CachedThingy newthingy = getSource().getNew(datatype);
      // Set up data for rendering the JSP 
      request.setAttribute(DATA, newthingy.getData());
      request.setAttribute(LOGGIE, newthingy.report());
      request.setAttribute(DATATYPE, datatype);
      request.setAttribute(KEY, "");
      RequestDispatcher rd = request.getRequestDispatcher("razz.jsp");
      rd.forward(request, response);
      return;
    }
    // Not a LOAD and not a NEW. It must be a SAVE. 
    // There should be data in the request.
    String data = request.getParameter(DATA);
    if (data == null || data.length() < 1) {
      throw new ServletException(
          "No \"DATA\" parameter found in POST, this servlet is unable to handle the request.\"");
    }
    LOG.debug("\"{}\" parameter found, the length is {}", DATA, data.length());

    // The data must be saved for a certain key value...
    String key = request.getParameter(KEY);
    if (key == null || key.length() < 1) {
      throw new ServletException(
          "No \"KEY\" parameter found in POST, this servlet is unable to handle the request.\"");
    }
    key = key.trim().toUpperCase();

    // Check to make sure the request was a SAVE command
    if (SAVE.equalsIgnoreCase(command)) {
      CachedThingy thingy = getSource().get(datatype, key);
      if (thingy == null) {
        throw new ServletException("Data thingy is null! This is impossible!");
      }
      thingy.setData(data);
      getSource().put(datatype, key, thingy);
    } else {
      throw new ServletException(String.format(
          "POST expects one of \"%s\", \"%s\" of \"%s\" for parameter \"%s\" but a \"%s\" was received!",
          LOAD, SAVE, NEW, COMMAND, command));
    }

    doGet(request, response);
  }

  private RazzSource getSource() throws ServletException {
    if( sourceInstance == null) {
      try {
        RazzSource src = new RazzSource();
        src.initConfiguration();
        synchronized ( RazzServlet.class) {
          sourceInstance = src;
        }
      } catch( Exception x) {
        throw new ServletException( x);
      }
    }
    return sourceInstance;
  } 

}
