<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    pageContext.setAttribute("key", request.getAttribute("key"));
    pageContext.setAttribute("data", request.getAttribute( "data"));
    pageContext.setAttribute("loggie", request.getAttribute( "loggie"));
    pageContext.setAttribute("datatype", request.getAttribute( "datatype"));
%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Vermillion Sands</title>

<!-- script>
	var key = '${key}';
	var data = '${data}';
	var message = '${message}';
</script-->       



</head>
<body onload="onLoad()">
<H1>Vermillion Sands</H1>

<form name="razz" action="razz" method="post" accept-charset="UTF-8">
	<input type="hidden" id="data" name="data" />
	<input type="hidden" name="command" id="command" value="niet geinitialiseerd"/>
	<p>
	Key: <input type="text" name="key" id="key" size="5" value="${key}" />
	&nbsp;&nbsp;&nbsp;&nbsp;
	<!--  De data type values moeten overeen komen met
	      de namen van de caches in cache.ccf  dus lowercase strings -->
	<input type="radio" name="datatype" value="upperletter" >ALFA
	<input type="radio" name="datatype" value="lowerletter">alfa
	<input type="radio" name="datatype" value="uppermixed">MIXED
	<input type="radio" name="datatype" value="lowermixed">mixed
	<input type="radio" name="datatype" value="numberonly">Number
	&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="button" value="load" onClick="doLoad()" >
	&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="button" value="save" onClick="doSave()" >
	&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="button" value="new" onClick="doNew()" >
	&nbsp;&nbsp;&nbsp;&nbsp;
	<a href="/cache">go to cache page</a>
	</p>
	<div style="width:100%; overflow:hidden;">
		<div style="width:600px; float:left;">
			DATA<br/>
			<textarea name="textsurface" id="textsurface" rows="32" cols="72">${data}
			</textarea>
		</div>
		<div style="margin-left:620px; text-align:left;">
			LOG<br/>
			${loggie}
		</div>
	</div>
</form>

<script>
function onLoad() {
	var datatype = "${datatype}";
	var radios = document.getElementsByName( 'datatype');
	for( var i=0; i<radios.length; i++) {
		if(radios[i].value == datatype) {
			radios[i].checked = true;
		}
	}
}

function doLoad() {
    var command = document.getElementById( "command");
    command.value = "load";
    document.razz.submit();
}

function doSave() {
    var command = document.getElementById( "command");
    command.value = "save";
    
    var textsurface = document.getElementById( "textsurface");
    var data  = document.getElementById( "data");
    data.value = textsurface.value;
    document.razz.submit();
}

function doNew() {
    var command = document.getElementById( "command");
    command.value = "new";
    document.razz.submit();
}


</script>
</body>
</html>