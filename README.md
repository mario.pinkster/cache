This is a Java memory cache.

Its features are:

* Adaptive LRU algorithm for cleanup. When the cache is full, cleanup removes multiple items.
* Cleanup runs in an application context thread. There is no separate background cleanup thread.
* Strictly memory cache. There is no overflow to disk.
* Webinterface (separate from the core) to monitor the caches and adjust parameters at runtime.
* No configuration file, config is set by calling methods on the cache objects.

More info on the general workings of the cache is in **webinterface/src/main/resources/\*.md**

The pom on the main level refers to these Maven modules:

* core - this is the core code for the cache.
* webinterface - this is the webinterface to monitor and adjust cache parameters at runtime.
* razz - this is an example webapplication that uses both the core and the webinterface.
* stress - this is a command line application to stress test the cache by making http calls to the razz application.

More information on the various modules are in the readme files for the subprojects.
